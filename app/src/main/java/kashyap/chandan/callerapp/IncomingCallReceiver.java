package kashyap.chandan.callerapp;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.telecom.TelecomManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import kashyap.chandan.callerapp.database.DataBaseHandler;

public class IncomingCallReceiver extends BroadcastReceiver {
    String incomingNumber;
    AudioManager audioManager;
    TelephonyManager telephonyManager;
    String checkNumber;
    String incoming;
    Contacts contacts;
    @SuppressLint("MissingPermission")
    @Override
    public void onReceive(Context context, Intent intent) {
        audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        // Get TelephonyManager
        telephonyManager= (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (intent.getAction().equals("android.intent.action.PHONE_STATE"))  {
            String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            if (state.equals("RINGING"))
            {
                telephonyManager.listen(new PhoneStateListener(){
                    @Override
                    public void onCallStateChanged(int state, String phoneNumber) {
                        super.onCallStateChanged(state, phoneNumber);
                        final DataBaseHandler[] db = {new DataBaseHandler(context)};
                        if(!phoneNumber.equals(""))
                        {
                            HandlerThread handlerThread =  new HandlerThread("database_helper");
                            handlerThread.start();
                            Handler handler =  new Handler(handlerThread.getLooper());
                            handler.post(new Runnable()
                            {
                                @Override
                                public void run() {

                                    db[0] =new DataBaseHandler(context);
                                    boolean result=db[0].getRejectedNumber(phoneNumber);
                                    if (result)
                                    {
                                           if (Build.VERSION.SDK_INT>=28)
                                           {
                                               TelecomManager telecomManager = (TelecomManager) context.getSystemService(Context.TELECOM_SERVICE);
                                               telecomManager.endCall();
                                               Contacts contact=db[0].getRejectedContact(phoneNumber);
                                               db[0].addNotification(contact.getName(),contact.getPhone(),getDate(),getTime());
                                           }
                                           else
                                           {
                                               try {
                                                   TelephonyManager tm= (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                                                   Class classTelephony = Class.forName(tm.getClass().getName());
                                                   Method method = classTelephony.getDeclaredMethod("getITelephony");
                                                   method.setAccessible(true);
                                                   // Invoke getITelephony() to get the ITelephony interface
                                                   com.android.internal.telephony.ITelephony telephonyInterface = (com.android.internal.telephony.ITelephony) method.invoke(telephonyManager);
                                                   // Get the endCall method from ITelephony
//                                                Class<?> telephonyInterfaceClass =Class.forName(telephonyInterface.getClass().getName());
//                                                Method methodEndCall = telephonyInterfaceClass.getDeclaredMethod("endCall");
//                                                // Invoke endCall()
//                                                methodEndCall.invoke(telephonyInterface);
                                                   telephonyInterface.endCall();
                                                   Contacts contact=db[0].getRejectedContact(phoneNumber);
                                                   db[0].addNotification(contact.getName(),contact.getPhone(),getDate(),getTime());
                                                   System.out.println("On Reject Call");
                                               }
                                               catch (Exception e) {
                                                   // TODO Auto-generated catch block
                                                   e.printStackTrace();
                                                   Log.i("EXCEPTION","Exception");
                                               }
                                           }
                                    }
                                    else
                                    {
                                        boolean notRing=db[0].getRejectedNumber(phoneNumber.substring(3));
                                        if (notRing)
                                        {
                                            if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.P)
                                            {
                                                TelecomManager telecomManager = (TelecomManager) context.getSystemService(Context.TELECOM_SERVICE);
                                                telecomManager.endCall();
                                                Contacts contact=db[0].getRejectedContact(phoneNumber);
                                                db[0].addNotification(contact.getName(),contact.getPhone(),getDate(),getTime());
                                            }
                                            else
                                            {
                                                try {

                                                    TelephonyManager tm= (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                                                    Class classTelephony = Class.forName(tm.getClass().getName());
                                                    Method method = classTelephony.getDeclaredMethod("endCall");
                                                    // Disable access check
                                                    method.setAccessible(true);
                                                    // Invoke getITelephony() to get the ITelephony interface
                                                    com.android.internal.telephony.ITelephony telephonyInterface = (com.android.internal.telephony.ITelephony) method.invoke(telephonyManager);
                                                    // Get the endCall method from ITelephony
//                                                Class<?> telephonyInterfaceClass =Class.forName(telephonyInterface.getClass().getName());
//                                                Method methodEndCall = telephonyInterfaceClass.getDeclaredMethod("endCall");
//                                                // Invoke endCall()
//                                                methodEndCall.invoke(telephonyInterface);
                                                    telephonyInterface.endCall();
                                                    System.out.println("On Reject Call");
                                                } catch (Exception e) {
                                                    // TODO Auto-generated catch block
                                                    e.printStackTrace();
                                                    Log.i("EXCEPTION","Exception");
                                                }
                                            }



                                            audioManager.setStreamMute(AudioManager.STREAM_RING,  true);
                                        }


                                        else
                                        {
//                                            audioManager.setStreamMute(AudioManager.STREAM_RING,  false);

                                            audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                                        }
//
                                    }
                                }});
                        handlerThread.quit();} }},PhoneStateListener.LISTEN_CALL_STATE); } } }


    private   String getDate()
    {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        return formattedDate;
    }
    private String getTime()
    {
        String pattern = "hh:mm:ss a";
        DateFormat dateFormat = new SimpleDateFormat(pattern);
        Calendar calToday = Calendar.getInstance();
        return dateFormat.format(calToday.getTime()) ;
    }
}
