package kashyap.chandan.callerapp.logins;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.lang.annotation.Annotation;

import kashyap.chandan.callerapp.R;
import kashyap.chandan.callerapp.retrofit.APIInterface;
import kashyap.chandan.callerapp.retrofit.ApiClient;
import kashyap.chandan.callerapp.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class OTPVerification extends AppCompatActivity implements View.OnClickListener {
    TextView tvTimer,otpSubmit;
    EditText otp1,otp2,otp3,otp4,otp5,otp6;
    Intent intent;
    ImageView iv_back;
    Dialog dialog;
    Bundle bundle;
    String id;
    GetOtpResponse.DataBean dataBean;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_o_t_p_verification);
        init();
        if (intent!=null)
        {
          bundle=intent.getBundleExtra("bundle");
          dataBean= (GetOtpResponse.DataBean) bundle.getSerializable("data");
        }
        otp1.requestFocus();
        requestFocus();
        otpSubmit.setOnClickListener(this);
        iv_back.setOnClickListener(this);
    }
    private void init() {
        intent=getIntent();
        dialog=new Dialog(OTPVerification.this);
        otpSubmit=findViewById(R.id.otpSubmit);
        otp1=findViewById(R.id.otp1);
        otp2=findViewById(R.id.otp2);
        otp3=findViewById(R.id.otp3);
        otp4=findViewById(R.id.otp4);
        otp5=findViewById(R.id.otp5);
        otp6=findViewById(R.id.otp6);
        iv_back=findViewById(R.id.iv_back);
    }
    private void requestFocus() {
        otp1.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(otp1.getText().toString().length()==1)     //size is your limit
                {
                    otp2.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        otp2.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(otp2.getText().toString().length()==1)     //size is your limit
                {
                    otp3.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        otp3.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(otp3.getText().toString().length()==1)     //size is your limit
                {
                    otp4.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        otp4.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(otp4.getText().toString().length()==1)     //size is your limit
                {
                    otp5.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        otp5.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(otp5.getText().toString().length()==1)     //size is your limit
                {
                    otp6.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.otpSubmit:
setOtpSubmit();

                break;
            case  R.id.iv_back:
                finish();
                break;
        }
    }
    private void setOtpSubmit()
    {
//        StringBuffer stringBuffer=otp1.getText().append(otp2.getText().append(otp3.getText().append(otp4.getText())));
        String otp_1=otp1.getText().toString();
        String otp_2=otp2.getText().toString();
        String otp_3=otp3.getText().toString();
        String otp_4=otp4.getText().toString();
        String otp_5=otp5.getText().toString();
        String otp_6=otp6.getText().toString();
        StringBuffer sb=new StringBuffer();
        sb.append(otp_1).append(otp_2).append(otp_3).append(otp_4).append(otp_5).append(otp_6);
        String otp= String.valueOf(sb);
        if (otp.length()<4)
        {
            Toast.makeText(this, "Enter Valid Otp", Toast.LENGTH_SHORT).show();
        }
        else
        {
            dialog.setContentView(R.layout.loadingdialog);
            dialog.setCancelable(true);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
            Call<VerifyOTPResponse>call=apiInterface.verifyOtp(otp,dataBean.getEmail(),dataBean.getRole());
            call.enqueue(new Callback<VerifyOTPResponse>() {
                @Override
                public void onResponse(Call<VerifyOTPResponse> call, Response<VerifyOTPResponse> response) {
                    if (response.code()==200)
                    {
                        dialog.dismiss();
                        Toast.makeText(OTPVerification.this, "OTP verification Successfull", Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(OTPVerification.this,SetPassword.class);
                        intent.putExtra("bundle",bundle);
                        startActivity(intent);
                        finish();
                    }
                    else
                    {
                        dialog.dismiss();
                        Converter<ResponseBody, ApiError> converter =
                                ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                        ApiError error;
                        try {
                            error = converter.convert(response.errorBody());
                            ApiError.StatusBean status=error.getStatus();
                            Toast.makeText(OTPVerification.this, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                        } catch (IOException e) { e.printStackTrace(); }
                    }
                }

                @Override
                public void onFailure(Call<VerifyOTPResponse> call, Throwable t) {

                }
            });
        }


    }
}