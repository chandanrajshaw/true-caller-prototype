package kashyap.chandan.callerapp.logins;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public  class GetOtpResponse implements Serializable {


    @Expose
    @SerializedName("status")
    private StatusBean status;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public static class StatusBean implements Serializable {
        @Expose
        @SerializedName("data")
        private DataBean data;
        @Expose
        @SerializedName("message")
        private String message;
        @Expose
        @SerializedName("code")
        private int code;

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }
    }

    public static class DataBean implements Serializable {
        @Expose
        @SerializedName("date_time_user")
        private String date_time_user;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("role")
        private String role;
        @Expose
        @SerializedName("otp")
        private String otp;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("u_id")
        private String u_id;
        @Expose
        @SerializedName("password")
        private String password;
        @Expose
        @SerializedName("original_pass")
        private String original_pass;
        @Expose
        @SerializedName("phone")
        private String phone;
        @Expose
        @SerializedName("email")
        private String email;
        @Expose
        @SerializedName("username")
        private String username;
        @Expose
        @SerializedName("id")
        private String id;

        public String getDate_time_user() {
            return date_time_user;
        }

        public void setDate_time_user(String date_time_user) {
            this.date_time_user = date_time_user;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getU_id() {
            return u_id;
        }

        public void setU_id(String u_id) {
            this.u_id = u_id;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getOriginal_pass() {
            return original_pass;
        }

        public void setOriginal_pass(String original_pass) {
            this.original_pass = original_pass;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
