package kashyap.chandan.callerapp.logins;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.regex.Pattern;

import kashyap.chandan.callerapp.R;
import kashyap.chandan.callerapp.retrofit.APIInterface;
import kashyap.chandan.callerapp.retrofit.ApiClient;
import kashyap.chandan.callerapp.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class ForgetPassword extends AppCompatActivity implements View.OnClickListener {
TextInputEditText et_emai;
TextView btnSubmit;
ImageView iv_back;
Dialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        init();
        iv_back.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
    }

    private void init() {
        dialog=new Dialog(ForgetPassword.this);
        iv_back=findViewById(R.id.iv_back);
        et_emai=findViewById(R.id.et_emai);
        btnSubmit=findViewById(R.id.btnSubmit);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case  R.id.iv_back:
                finish();
                break;
            case  R.id.btnSubmit:
               getOtp();
                break;

        }
    }
    public void showToast(String text)
    {
        Toast toast= Toast.makeText(getApplicationContext(),
                ""+text, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP| Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }
    public boolean emailValidation(String email)
    {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email.isEmpty()||email == null)
            return false;
        return pat.matcher(email).matches();
    }
  private   void getOtp()
    {
        String email=et_emai.getText().toString().trim();
        if (!emailValidation(email))
            showToast("Enter Valid Email");
        else
        {
            dialog.setContentView(R.layout.loadingdialog);
            dialog.setCancelable(true);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
            Call<GetOtpResponse>call=apiInterface.sendOTP(email);
            call.enqueue(new Callback<GetOtpResponse>() {
                @Override
                public void onResponse(Call<GetOtpResponse> call, Response<GetOtpResponse> response) {
                    if (response.code()==200)
                    {
                        Intent intent=new Intent(ForgetPassword.this,OTPVerification.class);
                        Bundle bundle=new Bundle();
                        bundle.putSerializable("data", (Serializable) response.body().getStatus().getData());
                        intent.putExtra("bundle",bundle);
                        startActivity(intent);
                        finish();
                    }
                    else
                    {
                        dialog.dismiss();
                        Converter<ResponseBody, ApiError> converter =
                                ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                        ApiError error;
                        try {
                            error = converter.convert(response.errorBody());
                            ApiError.StatusBean status=error.getStatus();
                            showToast(status.getMessage());
                        } catch (IOException e) { e.printStackTrace(); }
                    }
                }

                @Override
                public void onFailure(Call<GetOtpResponse> call, Throwable t) {

                }
            });
        }

    }
}