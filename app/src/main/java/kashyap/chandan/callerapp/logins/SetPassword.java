package kashyap.chandan.callerapp.logins;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.lang.annotation.Annotation;

import kashyap.chandan.callerapp.MainActivity;
import kashyap.chandan.callerapp.R;
import kashyap.chandan.callerapp.profile.MySettingPage;
import kashyap.chandan.callerapp.retrofit.APIInterface;
import kashyap.chandan.callerapp.retrofit.ApiClient;
import kashyap.chandan.callerapp.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class SetPassword extends AppCompatActivity implements View.OnClickListener {
TextView btnsetPwd;
    ImageView iv_back;
    Intent intent;
    Bundle bundle;
    TextInputEditText et_Con_new_pass,et_new_pass;
    Dialog dialog;
    GetOtpResponse.DataBean dataBean;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_password);
        init();
        if (intent!=null)
        {
            bundle=intent.getBundleExtra("bundle");
            dataBean= (GetOtpResponse.DataBean) bundle.getSerializable("data");
        }
        iv_back.setOnClickListener(this);
        btnsetPwd.setOnClickListener(this);
    }
    private void init() {
        intent=getIntent();

        et_Con_new_pass=findViewById(R.id.et_Con_new_pass);
        btnsetPwd=findViewById(R.id.btnsetPwd);
        et_new_pass=findViewById(R.id.et_new_pass);
        iv_back=findViewById(R.id.iv_back);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case  R.id.iv_back:
                finish();
                break;
            case  R.id.btnsetPwd:
               setPassword();
                break;

        }
    }
    private void setPassword()
    {
String newPass=et_new_pass.getText().toString().trim();
String conPass=et_Con_new_pass.getText().toString().trim();
if (newPass.isEmpty()&& conPass.isEmpty())
   showToast("Fill all fields");
else if (newPass.isEmpty())
    showToast("Enter New Password");
else if (conPass.isEmpty())
    showToast("Re-enter Password");
else if (!conPass.equals(newPass))
    showToast("Enter Password Carefully");
else
{
    dialog=new Dialog(SetPassword.this);
    dialog.setContentView(R.layout.loadingdialog);
    dialog.setCancelable(true);
    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    dialog.show();
    APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
    Call<SetPasswordResponse>call=apiInterface.setPassword(newPass,dataBean.getEmail(),dataBean.getRole());
    call.enqueue(new Callback<SetPasswordResponse>() {
        @Override
        public void onResponse(Call<SetPasswordResponse> call, Response<SetPasswordResponse> response) {
            if (response.code()==200) {
                dialog.dismiss();
                showToast("Password Changed Successfully");
                Intent intent = new Intent(SetPassword.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
            else
            {
                dialog.dismiss();
                Converter<ResponseBody, ApiError> converter =
                        ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                ApiError error;
                try {
                    error = converter.convert(response.errorBody());
                    ApiError.StatusBean status=error.getStatus();
                    Toast.makeText(SetPassword.this, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (IOException e) { e.printStackTrace(); }
            }
        }

        @Override
        public void onFailure(Call<SetPasswordResponse> call, Throwable t) {

        }
    });
}
    }
    public void showToast(String text)
    {
        Toast toast= Toast.makeText(getApplicationContext(),
                ""+text, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP| Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }
}