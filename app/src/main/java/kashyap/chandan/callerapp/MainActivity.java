package kashyap.chandan.callerapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.regex.Pattern;

import kashyap.chandan.callerapp.Admin.AdminDashBoard;
import kashyap.chandan.callerapp.customer.CustomerDashBoard;
import kashyap.chandan.callerapp.loginResponses.LoginResponse;
import kashyap.chandan.callerapp.logins.ForgetPassword;
import kashyap.chandan.callerapp.paffolder.Dashboard;
import kashyap.chandan.callerapp.retrofit.APIInterface;
import kashyap.chandan.callerapp.retrofit.ApiClient;
import kashyap.chandan.callerapp.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int ON_DO_NOT_DISTURB_CALLBACK_CODE =10 ;
    TextView btnLogin,forgetPassword;
    private static final int PHONE_STATE_PERMISSION = 1;
    Dialog dialog;
    TextInputEditText et_Username,et_password;
    Thread loginThread;
    SharedPreferenceData preferenceData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getApplicationContext(),Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED
                &&ContextCompat.checkSelfPermission(getApplicationContext(),Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED
                &&ContextCompat.checkSelfPermission(getApplicationContext(),Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED
                &&ContextCompat.checkSelfPermission(getApplicationContext(),Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED
                &&ContextCompat.checkSelfPermission(getApplicationContext(),Manifest.permission.ANSWER_PHONE_CALLS) != PackageManager.PERMISSION_GRANTED

        )
        {
            requestPermissions(new String[]{
                    Manifest.permission.READ_CONTACTS,
                    Manifest.permission.CALL_PHONE,
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.READ_CALL_LOG,
                    Manifest.permission.READ_PHONE_NUMBERS,
                    Manifest.permission.ANSWER_PHONE_CALLS,
            }, PHONE_STATE_PERMISSION);
        }
        if (Build.VERSION.SDK_INT>=23)
        {
requestForDoNotDisturbPermissionOrSetDoNotDisturbForApi23AndUp();
        }

        btnLogin.setOnClickListener(this);
        forgetPassword.setOnClickListener(this);
    }
    private void requestForDoNotDisturbPermissionOrSetDoNotDisturbForApi23AndUp() {

        // if user granted access else ask for permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            if ( notificationManager.isNotificationPolicyAccessGranted()) {
            } else{
                // Open Setting screen to ask for permisssion
                Intent intent = new Intent(Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);
                startActivityForResult( intent, ON_DO_NOT_DISTURB_CALLBACK_CODE );
            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ON_DO_NOT_DISTURB_CALLBACK_CODE) {
            this.requestForDoNotDisturbPermissionOrSetDoNotDisturbForApi23AndUp();
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
        if (preferenceData.getRole()!=null)
        {
            if (preferenceData.getRole().equalsIgnoreCase("admin"))
            {
                Intent intent=new Intent(MainActivity.this,AdminDashBoard.class);
                startActivity(intent);
                finish();
            }
            else if (preferenceData.getRole().equalsIgnoreCase("customer"))
            {
                Intent intent=new Intent(MainActivity.this,CustomerDashBoard.class);
                startActivity(intent);
                finish();
            }
            else
            {
                Intent intent=new Intent(MainActivity.this, Dashboard.class);
                startActivity(intent);
                finish();
            }
        }
        else
        {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void init() {
        preferenceData=new SharedPreferenceData(MainActivity.this);
        dialog=new Dialog(MainActivity.this);
        btnLogin=findViewById(R.id.btnLogin);
        forgetPassword=findViewById(R.id.forgetPassword);
        et_password=findViewById(R.id.et_password);
        et_Username=findViewById(R.id.et_Username);
    }
    public boolean emailValidation(String email)
    {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email.isEmpty()||email == null)
            return false;
        return pat.matcher(email).matches();
    }
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btnLogin:
                setBtnLogin();
                break;
            case R.id.forgetPassword:
                Intent intent1=new Intent(MainActivity.this, ForgetPassword.class);
                startActivity(intent1);
                break;
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode==PHONE_STATE_PERMISSION)
        {
            int length= grantResults.length;
            if (grantResults.length>0)
            {
                for (int i=0;i<length;i++)
                {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED)
                    {
                        // Granted. Start getting the location information
                    }
                    else if(grantResults[i]==PackageManager.PERMISSION_DENIED) {
                        {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext(), R.style.AlertDialogTheme);
                            builder.setTitle("Notice");
                            builder.setMessage("You Had Denied the Necessary Permissions.Please Go to app Setting and give All the permissions");
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                                    intent.setData(Uri.parse("package:" + getApplicationContext().getPackageName()));
                                    startActivity(intent);
                                }
                            });
                            builder.show();
                        }
                    }}}}
    }
    public void showToast(String text)
    {
        Toast toast= Toast.makeText(getApplicationContext(),
                ""+text, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP| Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }
    private void setBtnLogin()
    {
        String email=et_Username.getText().toString().trim();
        String password=et_password.getText().toString().trim();
        if (email.isEmpty()&&password.isEmpty())
            showToast("Fill All the fields");
        else if (!emailValidation(email))
            showToast("Enter Valid Email");
        else if (password.isEmpty())
            showToast("Enter Password");
        else
        {
            dialog.setContentView(R.layout.loadingdialog);
            dialog.setCancelable(true);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            APIInterface apiInterface=ApiClient.getClient().create(APIInterface.class);
            Call<LoginResponse>call=apiInterface.login(email,password);
            Runnable runnable=new Runnable() {
                @Override
                public void run() {
                    call.enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            if (response.code()==200)
                            {
                                dialog.dismiss();
                            LoginResponse.DetaBean detaBean=response.body().getDeta();
                            preferenceData.putSharedPreference(detaBean.getId(),detaBean.getUsername(),detaBean.getPhone(),detaBean.getEmail(),detaBean.getRole(),detaBean.getImage(),detaBean.getDate_time_user());
                                String role=preferenceData.getRole();
                            if (role.equalsIgnoreCase("admin"))
                            {
                               Intent intent=new Intent(MainActivity.this, AdminDashBoard.class);
                               startActivity(intent);
                               finish();
                            }
                            else if (role.equalsIgnoreCase("customer"))
                            {
                                Intent intent=new Intent(MainActivity.this,CustomerDashBoard.class);
                                startActivity(intent);
                                finish();
                            }
                            else
                            {
                                Intent intent=new Intent(MainActivity.this,Dashboard.class);
                                startActivity(intent);
                                finish();
                            }
                            }
                            else
                            {
                                dialog.dismiss();
                                Converter<ResponseBody, ApiError> converter =
                                        ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                ApiError error;
                                try {
                                    error = converter.convert(response.errorBody());
                                    ApiError.StatusBean status=error.getStatus();
                                   showToast(status.getMessage());
                                } catch (IOException e) { e.printStackTrace(); }
                            }
                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
dialog.dismiss();
showToast(t.getLocalizedMessage());
                        }
                    });
                }
            };
           loginThread=new Thread(runnable);
           loginThread.start();
        }

    }
}