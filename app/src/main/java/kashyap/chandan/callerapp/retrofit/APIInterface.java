package kashyap.chandan.callerapp.retrofit;


import java.util.List;

import kashyap.chandan.callerapp.Admin.AdminResponse.AddCustomerResponse;
import kashyap.chandan.callerapp.Admin.AdminResponse.CustomerListResponse;
import kashyap.chandan.callerapp.customer.AddPAResponse;
import kashyap.chandan.callerapp.customer.GetPAResponse;
import kashyap.chandan.callerapp.customer.SendContactResponse;
import kashyap.chandan.callerapp.loginResponses.LoginResponse;
import kashyap.chandan.callerapp.logins.GetOtpResponse;
import kashyap.chandan.callerapp.logins.SetPasswordResponse;
import kashyap.chandan.callerapp.logins.VerifyOTPResponse;
import kashyap.chandan.callerapp.paffolder.paresponses.AddBlueResponse;
import kashyap.chandan.callerapp.paffolder.paresponses.AddGreenResponse;
import kashyap.chandan.callerapp.paffolder.paresponses.AddRejectedResponse;
import kashyap.chandan.callerapp.paffolder.paresponses.BlueContactResponse;
import kashyap.chandan.callerapp.paffolder.paresponses.ContactStatusChangeResponse;
import kashyap.chandan.callerapp.paffolder.paresponses.GetContactResponse;
import kashyap.chandan.callerapp.paffolder.paresponses.GreenContactResponse;
import kashyap.chandan.callerapp.paffolder.paresponses.RejectedContactResponse;
import kashyap.chandan.callerapp.profile.UpdateProfileResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface APIInterface {
    @FormUrlEncoded
    @Headers("X-API-KEY:true@123")
    @POST("Loginservice/login")
    Call<LoginResponse> login(@Field("email") String email_phone,
                              @Field("password") String password);
    @FormUrlEncoded
    @Headers("X-API-KEY:true@123")
    @POST("Customerservices/add_customer")
    Call<AddCustomerResponse> add_customer(@Field("customer_name") String customer_name,
                                           @Field("email") String email,
                                    @Field("phone") String phone);

    @FormUrlEncoded
    @Headers("X-API-KEY:true@123")
    @POST("Loginservice/sendotp")
    Call<GetOtpResponse> sendOTP(
            @Field("email") String email);

    @FormUrlEncoded
    @Headers("X-API-KEY:true@123")
    @POST("Loginservice/forpass")
    Call<VerifyOTPResponse> verifyOtp( @Field("otp") String otp,
            @Field("email") String email,
                                       @Field("role") String role);

    @FormUrlEncoded
    @Headers("X-API-KEY:true@123")
    @POST("Loginservice/changepassotp")
    Call<SetPasswordResponse> setPassword(@Field("newpass") String newpass,
                                          @Field("email") String email,
                                          @Field("role") String role);

    @FormUrlEncoded
    @Headers("X-API-KEY:true@123")
    @POST("paservices/add_pa")
    Call<AddPAResponse> add_Pa(@Field("pa_name") String pa_name,
                               @Field("email") String email,
                               @Field("phone") String phone,
                               @Field("cust_id") String cust_id);

    @FormUrlEncoded
    @Headers("X-API-KEY:true@123")
    @POST("paservices/pa")
    Call<GetPAResponse> getPA(@Field("cust_id") String cust_id);


    @FormUrlEncoded
    @Headers("X-API-KEY:true@123")
    @POST("Contactservices/add_contact")
    Call<SendContactResponse> addContacts(@Field("name[]")List<String>name, @Field("phone[]")List<String>phone, @Field("id") String id);


//    @Headers("X-API-KEY:true@123")
//    @POST("Contactservices/add_contact")
//    Call<Contacts> addContacts( @Body JsonObject object);
@FormUrlEncoded
@Headers("X-API-KEY:true@123")
@POST("Contactservices/red_contact")
Call<AddRejectedResponse> addRejected(@Field("pa_id") String pa_id,@Field("contact_id[]") List<String> contact_id);

    @FormUrlEncoded
    @Headers("X-API-KEY:true@123")
    @POST("Contactservices/blue_contact")
    Call<AddBlueResponse> addBlue(@Field("pa_id") String pa_id,@Field("contact_id[]") List<String> contact_id);

    @FormUrlEncoded
    @Headers("X-API-KEY:true@123")
    @POST("Contactservices/green_contact")
    Call<AddGreenResponse> addGreen(@Field("pa_id") String pa_id,@Field("contact_id[]") List<String> contact_id);

    @FormUrlEncoded
    @Headers("X-API-KEY:true@123")
    @POST("Contactservices/getpacontact")
    Call<GetContactResponse> getContactList(@Field("pa_id")String id);

    @FormUrlEncoded
    @Headers("X-API-KEY:true@123")
    @POST("Contactservices/bluecontact")
    Call<BlueContactResponse> getBlueContact(@Field("pa_id")String id);

    @FormUrlEncoded
    @Headers("X-API-KEY:true@123")
    @POST("Contactservices/greencontact")
    Call<GreenContactResponse> getGreenContact(@Field("pa_id")String id);

    @FormUrlEncoded
    @Headers("X-API-KEY:true@123")
    @POST("Contactservices/redcontact")
    Call<RejectedContactResponse> getRejectedContact(@Field("pa_id")String id);

    @FormUrlEncoded
    @Headers("X-API-KEY:true@123")
    @POST("Contactservices/delete_contact")
    Call<ContactStatusChangeResponse> changeStatus(@Field("contact_id")String contact_id,@Field("pa_id")String id);


    @Headers("X-API-KEY:true@123")
    @GET("Customerservices/customer")
    Call<CustomerListResponse> getCustomerList();


    @Multipart
    @Headers("X-API-KEY:true@123")
    @POST("Loginservice/updateprofile")
    Call<UpdateProfileResponse>updateprofile(@Part("id") RequestBody id,
                                             @Part("name") RequestBody name,
                                             @Part("email") RequestBody email,
                                             @Part("phone") RequestBody phone,
                                             @Part("role") RequestBody role,
                                             @Part MultipartBody.Part images);




}
