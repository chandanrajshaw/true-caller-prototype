package kashyap.chandan.callerapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.callerapp.Contacts;

public class DataBaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "contactsManager";
    private static final String TABLE_REJECT = "RejectNumber";
    private static final String TABLE_BLUE = "BlueNumber";
    private static final String TABLE_GREEN = "GreenNumber";
    private static final String TABLE_NOTIFICATION = "Notification";

    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    Context context;

    private static final String KEY_PH_NO = "phone_number";
    private static final String KEY_DATE = "date";
    private static final String KEY_TIME = "time";
    private static final String CREATE_REJECT_TABLE = "CREATE TABLE " + TABLE_REJECT + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT" +
            " ," + KEY_NAME + " TEXT,"
            + KEY_PH_NO + " TEXT UNIQUE" + ")";
    private static final String CREATE_BLUE_TABLE = "CREATE TABLE " + TABLE_BLUE + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT" +
            " ," + KEY_NAME + " TEXT,"
            + KEY_PH_NO + " TEXT UNIQUE" + ")";
    private static final String CREATE_GREEN_TABLE = "CREATE TABLE " + TABLE_GREEN + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT" +
            " ," + KEY_NAME + " TEXT,"
            + KEY_PH_NO + " TEXT UNIQUE " + ")";
    private static final String CREATE_NOTIFICATION_TABLE = "CREATE TABLE " + TABLE_NOTIFICATION + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + " ," + KEY_NAME + " TEXT," + KEY_PH_NO + " TEXT " + ","+KEY_DATE+" TEXT,"+KEY_TIME+" TEXT)";

    public DataBaseHandler(@Nullable Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context=context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_REJECT_TABLE);
        db.execSQL(CREATE_GREEN_TABLE);
        db.execSQL(CREATE_BLUE_TABLE);
        db.execSQL(CREATE_NOTIFICATION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REJECT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GREEN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BLUE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTIFICATION);
        // Create tables again
        onCreate(db);
    }
    // code to add the new contact
    public void addNotification(String Name,String phone,String date,String time) {
        SQLiteDatabase db = this.getWritableDatabase();
        boolean is_Available=checkForRow(phone,TABLE_NOTIFICATION);
        if (is_Available)
        {
          int updated=updateNotification(date,time,phone);
        }
        else {
            ContentValues values = new ContentValues();
            values.put(KEY_NAME, Name); // Contact Name
            values.put(KEY_PH_NO, phone); // Contact Phone
            values.put(KEY_DATE, date);
            values.put(KEY_TIME, time);
            db.insert(TABLE_NOTIFICATION, null, values);
            db.close();
        }
    }
     public void addRejectedContact(Contacts contact) {
        SQLiteDatabase db = this.getWritableDatabase();
         boolean status=checkForRow(contact.getPhone(),TABLE_REJECT);
         if (!status)
         {
             ContentValues values = new ContentValues();
             values.put(KEY_NAME, contact.getName());
             values.put(KEY_PH_NO, contact.getPhone());
             db.insert(TABLE_REJECT, null, values);
             db.close();
             Log.i("INSERTED","INSERTED TO REJECTED");
         }
    }
    public void addBlueContact(Contacts contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        boolean status=checkForRow(contact.getPhone(),TABLE_BLUE);
        if (!status)
        {
            ContentValues values = new ContentValues();
            values.put(KEY_NAME, contact.getName()); // Contact Name
            values.put(KEY_PH_NO, contact.getPhone()); // Contact Phone
            db.insert(TABLE_BLUE, null, values);
            db.close();
            Log.i("INSERTED","INSERTED");
            System.out.println("INSERTED");
        }
    }
    public void addGreenContact(Contacts contact)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        boolean status=checkForRow(contact.getPhone(),TABLE_GREEN);
        if (!status)
        {
            ContentValues values = new ContentValues();
            values.put(KEY_NAME, contact.getName()); // Contact Name
            values.put(KEY_PH_NO, contact.getPhone()); // Contact Phone
            db.insert(TABLE_GREEN, null, values);
            db.close();
            Log.i("INSERTED","INSERTED");
            System.out.println("INSERTED");
        }
    }
    // code to get the single contact
   public boolean getRejectedNumber(String number) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_REJECT, new String[] { KEY_ID,
                        KEY_NAME, KEY_PH_NO }, KEY_PH_NO + "=?",
                new String[] { String.valueOf(number) }, null, null, null, null);
        if (cursor.getCount()<=0) return false;
        else return true;
    }
    public Contacts getRejectedContact(String number) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_REJECT, new String[]{KEY_ID,
                        KEY_NAME, KEY_PH_NO}, KEY_PH_NO + "=?",
                new String[]{String.valueOf(number)}, null, null,null, null);
            cursor.moveToFirst();
            Contacts contact = new Contacts(cursor.getInt(0),
                    cursor.getString(1), cursor.getString(2));
            // return contact
            return contact;
    }
    public boolean getBlueNumber(String number) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_BLUE, new String[] { KEY_ID,
                        KEY_NAME, KEY_PH_NO }, KEY_PH_NO + "=?",
                new String[] { String.valueOf(number) }, null, null, null, null);
        if (cursor.getCount()<=0) return false;
        else return true;

    }
    public boolean getGreenNumber(String number) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_GREEN, new String[] { KEY_ID,
                        KEY_NAME, KEY_PH_NO }, KEY_PH_NO + "=?",
                new String[] { String.valueOf(number) }, null, null, null, null);
        if (cursor.getCount()<=0) return false;
        else return true;
    }
    public List<Contacts> getRejectedTable() {
        List<Contacts> contactList = new ArrayList<Contacts>();
        String selectQuery = "SELECT  * FROM " + TABLE_REJECT;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
if (cursor.getCount()<=0)
{
    return contactList;
}
else
{
    if (cursor.moveToFirst()) {
        do {
            Contacts contact = new Contacts();
            contact.setId(cursor.getString(0));
            contact.setName(cursor.getString(1));
            contact.setPhone(cursor.getString(2));
            contactList.add(contact);
        } while (cursor.moveToNext());
    }
    return contactList;
}

    }
    public List<Notifications> getNotificationTable() {
        List<Notifications> notificationsList = new ArrayList<Notifications>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NOTIFICATION;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
if (cursor.getCount()<=0)
{
    return notificationsList;
}
else {
    if (cursor.moveToFirst()) {
        do {
            Notifications notifications = new Notifications();
            notifications.setId(cursor.getString(0));
            notifications.setName(cursor.getString(1));
            notifications.setPhone(cursor.getString(2));
            notifications.setDate(cursor.getString(3));
            notifications.setTime(cursor.getString(4));
            notificationsList.add(notifications);
        } while (cursor.moveToNext());

    }
    return notificationsList;
} }
    public List<Contacts> getBlueTable() {
        List<Contacts> contactList = new ArrayList<Contacts>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_BLUE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Contacts contact = new Contacts();
                contact.setId(cursor.getString(0));
                contact.setName(cursor.getString(1));
                contact.setPhone(cursor.getString(2));
                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        // return contact list
        return contactList;
    }
    public List<Contacts> getGreenTable()
    {
        List<Contacts> contactList = new ArrayList<Contacts>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_GREEN;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Contacts contact = new Contacts();
                contact.setId(cursor.getString(0));
                contact.setName(cursor.getString(1));
                contact.setPhone(cursor.getString(2));
                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        // return contact list
        return contactList;
    }
    // code to update the single contact
    public int updateNotification(String date,String time,String phone) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_DATE, date);
        values.put(KEY_TIME, time);

        // updating row
        return db.update(TABLE_NOTIFICATION, values, KEY_PH_NO + " = ?",
                new String[] { phone });
    }

    // Deleting single contact
    public int deleteRejected(String phone) {
        SQLiteDatabase db = this.getWritableDatabase();
       int del= db.delete(TABLE_REJECT, KEY_PH_NO + " = ?",
                new String[] { phone});
        db.close();
        return del;
    }
    public int deleteBlue(String phone) {
        SQLiteDatabase db = this.getWritableDatabase();
        int del= db.delete(TABLE_BLUE, KEY_PH_NO + " = ?",
                new String[] { phone});
        db.close();
        return del;
    }
    public int deleteGreen(String phone) {
        SQLiteDatabase db = this.getWritableDatabase();
        int del= db.delete(TABLE_GREEN, KEY_PH_NO + " = ?",
                new String[] { phone});
        db.close();
        return del;
    }
    public int getContactsCountRejected() {
        String countQuery = "SELECT  * FROM " + TABLE_REJECT;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
        return cursor.getCount();
    }
    public int getContactsCountBlue() {
        String countQuery = "SELECT  * FROM " + TABLE_BLUE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
        return cursor.getCount();
    }
    public int getContactsCountGreen() {
        String countQuery = "SELECT  * FROM " + TABLE_GREEN;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
        return cursor.getCount();
    }
    public boolean checkForRow(String number,String TABLENAME)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLENAME, new String[] { KEY_ID,
                        KEY_NAME, KEY_PH_NO }, KEY_PH_NO + "=?",
                new String[] { String.valueOf(number) }, null, null, null, null);
        if (cursor.getCount()<=0) return false;
        else return true;
    }
}
