package kashyap.chandan.callerapp.profile;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ActionBar;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.callerapp.MainActivity;
import kashyap.chandan.callerapp.R;
import kashyap.chandan.callerapp.SharedPreferenceData;
import kashyap.chandan.callerapp.retrofit.APIInterface;
import kashyap.chandan.callerapp.retrofit.ApiClient;
import kashyap.chandan.callerapp.retrofit.ApiError;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class MySettingPage extends AppCompatActivity {
    private static final int FILE_PERMISSION =10 ;
    SharedPreferenceData sharedPreferenceData;
TextView tvName,tvEmail,tvPhone;
CardView logout;
CircleImageView profileImage,profilePic;
ImageView iv_edit;
Dialog profileDialog,cameradialog,dialog;
    private String picturePath;
    Bitmap converetdImage;
    File image=null;
    MultipartBody.Part profileImg=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_setting_page);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvPhone.setText(sharedPreferenceData.getPhone());
        tvName.setText(sharedPreferenceData.getUserName());
        tvEmail.setText(sharedPreferenceData.getEmail());
        Picasso.get().load(ApiClient.IMAGE_URL+sharedPreferenceData.getImage()).placeholder(R.drawable.loading).error(R.drawable.ic_broken_image).into(profilePic);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharedPreferenceData.sessionEnd();
                Intent intent=new Intent(MySettingPage.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        iv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                profileDialog.setContentView(R.layout.profile_update_dialog);
                profileDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                profileDialog.setCancelable(true);
                CardView btnUpdate=profileDialog.findViewById(R.id.update);
                TextInputEditText et_name=profileDialog.findViewById(R.id.et_Username);
                TextInputEditText et_email=profileDialog.findViewById(R.id.et_email);
                TextInputEditText et_phone=profileDialog.findViewById(R.id.et_phone);
                ImageView close=profileDialog.findViewById(R.id.close);
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        profileDialog.dismiss();
                    }
                });
                profileImage=profileDialog.findViewById(R.id.profileImage);
                et_name.setText(sharedPreferenceData.getUserName());
                et_email.setText(sharedPreferenceData.getEmail());
                et_phone.setText(sharedPreferenceData.getPhone());
                profileImage.setOnClickListener(new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onClick(View view) {
                    chooseProfilePic();
                    }
                });
                btnUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String name=et_name.getText().toString().trim();
                        String email=et_email.getText().toString().trim();
                        String phone=et_phone.getText().toString().trim();
                        if (name.isEmpty()&&email.isEmpty()&&phone.isEmpty())
                            showToast("Fields can't be Empty");
                        else if (name.isEmpty())
                            showToast("Enter Username");
                        else if (!emailValidation(email))
                            showToast("Enter Valid Email");
                        else if (phone.isEmpty()||phone.length()!=10)
                            showToast("Enter Valid Mobile number");
                        else
                        {
                            if (image!=null)
                            {
                                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                                profileImg = MultipartBody.Part.createFormData("image", image.getName(), requestFile);
                            }
                            else {
                                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                                profileImg = MultipartBody.Part.createFormData("image", "", requestFile);
                            }
                            RequestBody rid = RequestBody.create(MediaType.parse("multipart/form-data"),sharedPreferenceData.getId());
                            RequestBody rName = RequestBody.create(MediaType.parse("multipart/form-data"),name);
                            RequestBody remail = RequestBody.create(MediaType.parse("multipart/form-data"),email);
                            RequestBody rPhone = RequestBody.create(MediaType.parse("multipart/form-data"),phone);
                            RequestBody rRole = RequestBody.create(MediaType.parse("multipart/form-data"),sharedPreferenceData.getRole());
                            dialog.setContentView(R.layout.loadingdialog);
                            dialog.setCancelable(true);
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            dialog.show();
                            APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                            Call<UpdateProfileResponse>call=apiInterface.updateprofile(rid,rName,remail,rPhone,rRole,profileImg);
                            call.enqueue(new Callback<UpdateProfileResponse>() {
                                @Override
                                public void onResponse(Call<UpdateProfileResponse> call, Response<UpdateProfileResponse> response) {
                                    if (response.code()==200)
                                    {
                                        dialog.dismiss();
                                        Intent intent=new Intent(MySettingPage.this,MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    }
                                    else
                                    {
                                        dialog.dismiss();
                                        Converter<ResponseBody, ApiError> converter =
                                                ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                        ApiError error;
                                        try {
                                            error = converter.convert(response.errorBody());
                                            ApiError.StatusBean status=error.getStatus();
                                            showToast(status.getMessage());
                                        } catch (IOException e) { e.printStackTrace(); }
                                    }
                                }

                                @Override
                                public void onFailure(Call<UpdateProfileResponse> call, Throwable t) {

                                }
                            });


                            /****************************************************
                             * ******************************************************
                             * ********************************************
                             */
                        }

                    }
                });
                profileDialog.show();
            }
        });
    }

    private void init() {
        dialog=new Dialog(MySettingPage.this);
        profilePic=findViewById(R.id.profileImage);
        profileDialog=new Dialog(MySettingPage.this);
        iv_edit=findViewById(R.id.iv_edit);
        sharedPreferenceData=new SharedPreferenceData(MySettingPage.this);
        tvEmail=findViewById(R.id.tvEmail);
        tvName=findViewById(R.id.tvName);
        tvPhone=findViewById(R.id.tvPhone);
        logout=findViewById(R.id.logout);

    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void chooseProfilePic()
    {
        if (ContextCompat.checkSelfPermission(MySettingPage.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED||
                ContextCompat.checkSelfPermission(MySettingPage.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED
                ||ContextCompat.checkSelfPermission(MySettingPage.this,Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED) {

            askPermission();
        }
        else {

            ImageView camera, folder;
            cameradialog = new Dialog(MySettingPage.this);
            cameradialog.setContentView(R.layout.dialogboxcamera);
            DisplayMetrics metrics=getResources().getDisplayMetrics();
            int width=metrics.widthPixels;
            cameradialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
            cameradialog.show();
            cameradialog.setCancelable(true);
            cameradialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Window window = cameradialog.getWindow();
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
            camera = cameradialog.findViewById(R.id.camera);
            folder = cameradialog.findViewById(R.id.gallery);
            folder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, 100);
                    cameradialog.dismiss();
                }
            });
            camera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, 101);
                    cameradialog.dismiss();
                }
            });
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);
        if (resultCode == RESULT_OK) {

            if (requestCode == 100  && resultData != null) {

//the image URI
                Uri selectedImage = resultData.getData();

                //     imagepath=selectedImage.getPath();

                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                cursor.close();


                if (picturePath != null && !picturePath.equals("")) {
                    image = new File(picturePath);
                }

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    converetdImage = getResizedBitmap(bitmap, 500);
                    profileImage.setImageBitmap(converetdImage);
                    profileImage.setVisibility(View.VISIBLE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else if (requestCode == 101 ) {
                Bitmap converetdImage = (Bitmap) resultData.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                profileImage.setImageBitmap(converetdImage);
                profileImage.setVisibility(View.VISIBLE);
                image = new File(Environment.getExternalStorageDirectory(), "ProfileImage.jpg");
                FileOutputStream fo;
                try {
                    fo = new FileOutputStream(image);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void askPermission() {
        if (ContextCompat.checkSelfPermission(MySettingPage.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(MySettingPage.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED
                ||ContextCompat.checkSelfPermission(MySettingPage.this,Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA},FILE_PERMISSION);
        }
    }
    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode==FILE_PERMISSION)
        {
            int length= grantResults.length;
            if (grantResults.length>0)
            {
                for (int i=0;i<length;i++)
                {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED)
                    {
                        // Granted. Start getting the location information
                    }
                    else if(grantResults[i]==PackageManager.PERMISSION_DENIED) {
                        {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext(), R.style.AlertDialogTheme);
                            builder.setTitle("Notice");
                            builder.setMessage("You Had Denied the Necessary Permissions.Please Go to app Setting and give All the permissions");
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                                    intent.setData(Uri.parse("package:" + getApplicationContext().getPackageName()));
                                    startActivity(intent);
                                }
                            });
                            builder.show();
                        }
                    }}}}
    }
    public void showToast(String text)
    {
        Toast toast= Toast.makeText(getApplicationContext(),
                ""+text, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP| Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }
    public boolean emailValidation(String email)
    {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email.isEmpty()||email == null)
            return false;
        return pat.matcher(email).matches();
    }
}