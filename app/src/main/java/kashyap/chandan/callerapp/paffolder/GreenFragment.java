package kashyap.chandan.callerapp.paffolder;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.callerapp.Contacts;
import kashyap.chandan.callerapp.CustomItemClickListener;
import kashyap.chandan.callerapp.R;
import kashyap.chandan.callerapp.SharedPreferenceData;
import kashyap.chandan.callerapp.database.DataBaseHandler;
import kashyap.chandan.callerapp.paffolder.paresponses.ContactStatusChangeResponse;
import kashyap.chandan.callerapp.paffolder.paresponses.GreenContactResponse;
import kashyap.chandan.callerapp.retrofit.APIInterface;
import kashyap.chandan.callerapp.retrofit.ApiClient;
import kashyap.chandan.callerapp.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class GreenFragment extends Fragment {

    Dashboard dashboard;
    SharedPreferenceData preferenceData;
    Dialog dialog;
    TextView tvNotice;
    GreenContactAdapter adapter;
    DataBaseHandler db;
    RecyclerView contactRecycler;
    List< GreenContactResponse.DataBean> greenContacts=new ArrayList<>();
    public GreenFragment(Dashboard dashboard) {

        this.dashboard = dashboard;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View view= inflater.inflate(R.layout.green_fragment,container,false);
        contactRecycler=view.findViewById(R.id.contactRecycler);
        tvNotice=view.findViewById(R.id.tvNotice);
        preferenceData=new SharedPreferenceData(dashboard);
        db=new DataBaseHandler(dashboard);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        contactRecycler.setLayoutManager(new LinearLayoutManager(dashboard,LinearLayoutManager.VERTICAL,false));
    }

    @Override
    public void onResume() {
        super.onResume();
        getGreenContacts();
    }
    private void getGreenContacts()
    {
        contactRecycler.setAdapter(null);
        dialog=new Dialog(dashboard);
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<GreenContactResponse> call=apiInterface.getGreenContact(preferenceData.getId());
        call.enqueue(new Callback<GreenContactResponse>() {
            @Override
            public void onResponse(Call<GreenContactResponse> call, Response<GreenContactResponse> response) {
                if (response.code()==200)
                {dialog.dismiss();
                    contactRecycler.setVisibility(View.VISIBLE);
                     tvNotice.setVisibility(View.GONE);
                   greenContacts=response.body().getData();
                   for (int i=0;i<greenContacts.size();i++)
                   {
                       GreenContactResponse.DataBean contact=greenContacts.get(i);
                       Contacts con=new Contacts();
                       con.setName(contact.getName());
                       con.setPhone(contact.getPhone());
                       db.addGreenContact(con);
                   }
                   adapter=new GreenContactAdapter(dashboard, greenContacts, new CustomItemClickListener() {
                       @Override
                       public void onItemClick(View v, String id, String position,String phone) {
changeContactStatus(id,position,phone);
                       }
                   });
                   contactRecycler.setAdapter(adapter);
                }
                else
                {
                    dialog.dismiss();
                    tvNotice.setVisibility(View.VISIBLE);
                    contactRecycler.setVisibility(View.GONE);
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        showToast(status.getMessage());
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<GreenContactResponse> call, Throwable t) {
                showToast(""+t.getMessage());
            }
        });
    }
    private void changeContactStatus(String id, String position,String phone) {
        dialog=new Dialog(dashboard);
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<ContactStatusChangeResponse> call=apiInterface.changeStatus(id,preferenceData.getId());
        call.enqueue(new Callback<ContactStatusChangeResponse>() {
            @Override
            public void onResponse(Call<ContactStatusChangeResponse> call, Response<ContactStatusChangeResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    adapter.updateList(Integer.parseInt(position));
                    showToast("Removed From Blue Contact");
                    int reject= db.deleteGreen(phone);
                    if (reject>0)
                        Toast.makeText(dashboard, "Deleted from Local", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(dashboard, "Not Deleted from Local", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        showToast(status.getMessage());
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<ContactStatusChangeResponse> call, Throwable t) {
                dialog.dismiss();showToast(t.getMessage());
            }
        });


    }
    public void showToast(String text)
    {
        Toast toast= Toast.makeText(dashboard,
                ""+text, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP| Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }
}
