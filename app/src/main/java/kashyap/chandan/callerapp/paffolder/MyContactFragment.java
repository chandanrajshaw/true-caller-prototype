package kashyap.chandan.callerapp.paffolder;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.callerapp.Contacts;
import kashyap.chandan.callerapp.R;
import kashyap.chandan.callerapp.SharedPreferenceData;
import kashyap.chandan.callerapp.customer.GetPAResponse;
import kashyap.chandan.callerapp.database.DataBaseHandler;
import kashyap.chandan.callerapp.paffolder.paresponses.AddBlueResponse;
import kashyap.chandan.callerapp.paffolder.paresponses.AddGreenResponse;
import kashyap.chandan.callerapp.paffolder.paresponses.AddRejectedResponse;
import kashyap.chandan.callerapp.paffolder.paresponses.GetContactResponse;
import kashyap.chandan.callerapp.retrofit.APIInterface;
import kashyap.chandan.callerapp.retrofit.ApiClient;
import kashyap.chandan.callerapp.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class MyContactFragment extends Fragment implements View.OnLongClickListener {
   public List<GetContactResponse.DataBean> selectUsers = new ArrayList<>();
    public static List<GetContactResponse.DataBean> selectedArrayList =new ArrayList<>();
    ArrayList<Contacts> contacts = new ArrayList<>();
    public ArrayList<Contacts> removed = new ArrayList<>();
    private RecyclerView contactRecycler;
    public  static PAContactAdapter adapter;
    Cursor phones;
    List<String>id=new ArrayList<>();
    DataBaseHandler dataBaseHandler;
    Context context;
    Dialog dialog;
    SharedPreferenceData sharedPreferenceData;
    Dashboard dashboard;
    public static boolean is_in_action_mode = false;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;

    public MyContactFragment(Context context) {
        this.context = context;
        dashboard = (Dashboard) context;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_contact_fragment, container, false);
        contactRecycler = view.findViewById(R.id.contactRecycler);
        setHasOptionsMenu(true);
        is_in_action_mode = false;
//        adapter = new ContactAdapter(getContext(), selectUsers);
        dataBaseHandler=new DataBaseHandler(dashboard);
        sharedPreferenceData=new SharedPreferenceData(dashboard);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        showContacts();
        contactRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        getContacts();
        Dashboard.iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyContactFragment.is_in_action_mode=false;
                id.clear();
              adapter.notifyDataSetChanged();
                Dashboard.iv_back.setVisibility(View.GONE);
                dashboard.toolbar.getMenu().clear();
                dashboard.toolbar.inflateMenu(R.menu.options_menu);
            }
        });
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
//                showContacts();
                getContacts();
            } else {
                Snackbar.make(getActivity().getWindow().getDecorView().findViewById(android.R.id.content), "Until you grant the permission, we canot display the names", Snackbar.LENGTH_SHORT).show();
            }
        }
    }




    @Override
    public boolean onLongClick(View view) {
        is_in_action_mode = true;
        dashboard.toolbar.getMenu().clear();
        dashboard.iv_back.setVisibility(View.VISIBLE);
        dashboard.toolbar.inflateMenu(R.menu.selection_toolbar_item);
        adapter.notifyDataSetChanged();
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                dashboard.toolbar.getMenu().clear();
//               activityActionBar.setDisplayHomeAsUpEnabled(false);
                dashboard.tvTitle.setText("Caller App");
                is_in_action_mode = false;
                adapter.notifyDataSetChanged();
                break;
            case R.id.reject:
                dashboard.toolbar.getMenu().clear();
                dashboard.toolbar.inflateMenu(R.menu.options_menu);
                MyContactFragment.is_in_action_mode=false;
                addRejected(selectedArrayList);
                adapter.notifyDataSetChanged();
                dashboard.iv_back.setVisibility(View.GONE);
               break;
            case R.id.green:

                dashboard.toolbar.getMenu().clear();
                dashboard.toolbar.inflateMenu(R.menu.options_menu);
                MyContactFragment.is_in_action_mode=false;
                addGreen(selectedArrayList);
                adapter.notifyDataSetChanged();
                dashboard.iv_back.setVisibility(View.GONE);
                break;
            case R.id.blue:
                dashboard.toolbar.getMenu().clear();
                dashboard.toolbar.inflateMenu(R.menu.options_menu);
                MyContactFragment.is_in_action_mode=false;
                addBlue(selectedArrayList);
                adapter.notifyDataSetChanged();
                dashboard.iv_back.setVisibility(View.GONE);
                break;
        }
        return true;
    }


    private void getContacts()
    {
        dialog=new Dialog(dashboard);
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<GetContactResponse>call=apiInterface.getContactList(sharedPreferenceData.getId());
        call.enqueue(new Callback<GetContactResponse>() {
            @Override
            public void onResponse(Call<GetContactResponse> call, Response<GetContactResponse> response) {
                if (response.code()==200)
                {
dialog.dismiss();
selectUsers=response.body().getData();
                    adapter = new PAContactAdapter(getContext(), selectUsers);
                    contactRecycler.setAdapter(adapter);
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        showToast(status.getMessage());
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<GetContactResponse> call, Throwable t) {

            }
        });
    }
    public void prepareSelection(View view, GetContactResponse.DataBean contacts) {
        if (((CheckBox) view).isChecked()) {
            selectedArrayList.add(contacts);
        } else {
            selectedArrayList.remove(contacts);
        }
    }
    private void addRejected(List<GetContactResponse.DataBean> list)
    {
        if (list.size()<=0)
        {
            Toast.makeText(context, "Please select the contact", Toast.LENGTH_SHORT).show();
        }
        else
        {
           for (int i=0;i<list.size();i++)
           {
               GetContactResponse.DataBean contacts=list.get(i);
              id.add(contacts.getId());
           }
           dialog=new Dialog(dashboard);
            dialog.setContentView(R.layout.loadingdialog);
            dialog.setCancelable(true);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
            Call<AddRejectedResponse>call=apiInterface.addRejected(sharedPreferenceData.getId(),id);
            call.enqueue(new Callback<AddRejectedResponse>() {
                @Override
                public void onResponse(Call<AddRejectedResponse> call, Response<AddRejectedResponse> response) {
                    if (response.code()==200)
                    {
                        dialog.dismiss();
                        for (GetContactResponse.DataBean contacts: list) {
                            Contacts contacts1=new Contacts();
                            contacts1.setName(contacts.getName());
                            contacts1.setPhone(contacts.getPhone());
                            dataBaseHandler.addRejectedContact(contacts1);
                        }
                    showToast("Added to Rejected List");
                        selectedArrayList.clear();
                        id.clear();
                    }
                    else
                    {
                        selectedArrayList.clear();
                        dialog.dismiss();
                        Converter<ResponseBody, ApiError> converter =
                                ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                        ApiError error;
                        try {
                            error = converter.convert(response.errorBody());
                            ApiError.StatusBean status=error.getStatus();
                            showToast(status.getMessage());
                        } catch (IOException e) { e.printStackTrace(); }
                        id.clear();
                    }
                }

                @Override
                public void onFailure(Call<AddRejectedResponse> call, Throwable t) {
                    selectedArrayList.clear();
                    id.clear();
                }
            });
        }
    }
    private void addGreen(List<GetContactResponse.DataBean> list)
    {
        if (list.size()<=0)
        {
            Toast.makeText(context, "Please select the contact", Toast.LENGTH_SHORT).show();
        }
        else
        {


            for (int i=0;i<list.size();i++)
            {
                GetContactResponse.DataBean contacts=list.get(i);
                id.add(contacts.getId());
            }
            dialog=new Dialog(dashboard);
            dialog.setContentView(R.layout.loadingdialog);
            dialog.setCancelable(true);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
            Call<AddGreenResponse>call=apiInterface.addGreen(sharedPreferenceData.getId(),id);
            call.enqueue(new Callback<AddGreenResponse>() {
                @Override
                public void onResponse(Call<AddGreenResponse> call, Response<AddGreenResponse> response) {
                    if (response.code()==200)
                    {
                        for (GetContactResponse.DataBean contacts: list) {
                            Contacts con=new Contacts();
                            con.setPhone(contacts.getPhone());
                            con.setName(contacts.getName());
                            dataBaseHandler.addGreenContact(con);
                        }
                        showToast("Added to Green List");
                        dialog.dismiss();
                        selectedArrayList.clear();
                       id.clear();
                    }
                    else
                    {
                        id.clear();
                        dialog.dismiss();
                        selectedArrayList.clear();
                        Converter<ResponseBody, ApiError> converter =
                                ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                        ApiError error;
                        try {
                            error = converter.convert(response.errorBody());
                            ApiError.StatusBean status=error.getStatus();
                            showToast(status.getMessage());
                        } catch (IOException e) { e.printStackTrace(); }
                    }
                }

                @Override
                public void onFailure(Call<AddGreenResponse> call, Throwable t) {
dialog.dismiss();
                    id.clear();
                }
            });
        }
    }
    private void addBlue(List<GetContactResponse.DataBean> list)
    {
        if (list.size()<=0)
        {
            Toast.makeText(context, "Please select the contact", Toast.LENGTH_SHORT).show();
        }
        else
        {
            for (int i=0;i<list.size();i++)
            {
                GetContactResponse.DataBean contacts=list.get(i);

                id.add(contacts.getId());
            }
            dialog=new Dialog(dashboard);
            dialog.setContentView(R.layout.loadingdialog);
            dialog.setCancelable(true);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
            Call<AddBlueResponse>call=apiInterface.addBlue(sharedPreferenceData.getId(),id);
            call.enqueue(new Callback<AddBlueResponse>() {
                @Override
                public void onResponse(Call<AddBlueResponse> call, Response<AddBlueResponse> response) {
                    if (response.code()==200)
                    {

                        for (GetContactResponse.DataBean contacts: list) {
                            {
                                Contacts con=new Contacts();
                                con.setName(contacts.getName());
                                con.setPhone(contacts.getName());
                                dataBaseHandler.addBlueContact(con);
                            }
                        }
                        dialog.dismiss();
                        showToast("Added to Blue List");
                        selectedArrayList.clear();
                        id.clear();
                    }
                    else
                    {
                        dialog.dismiss();
                        Converter<ResponseBody, ApiError> converter =
                                ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                        ApiError error;
                        try {
                            error = converter.convert(response.errorBody());
                            ApiError.StatusBean status=error.getStatus();
                            showToast(status.getMessage());
                        } catch (IOException e) { e.printStackTrace(); }
                    }
                    id.clear();
                }

                @Override
                public void onFailure(Call<AddBlueResponse> call, Throwable t) {
id.clear();
showToast(t.getMessage());
                }
            });
        }
    }
    public void showToast(String text)
    {
        Toast toast= Toast.makeText(dashboard,
                ""+text, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP| Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }
}
