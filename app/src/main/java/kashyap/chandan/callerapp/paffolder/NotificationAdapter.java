package kashyap.chandan.callerapp.paffolder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.callerapp.CustomItemClickListener;
import kashyap.chandan.callerapp.R;
import kashyap.chandan.callerapp.database.Notifications;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
    Context context;
    List<Notifications> notifications;
    public NotificationAdapter(Context context, List<Notifications> notifications, CustomItemClickListener listener) {
        this.context=context;
        this.notifications=notifications;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.notification_list_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
holder.date.setText(notifications.get(position).getDate());
        holder.time.setText(notifications.get(position).getTime());
        holder.contact_number.setText(notifications.get(position).getPhone());
        holder.contact_name.setText(notifications.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_call;
        TextView date,contact_name,contact_number,time;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            date=itemView.findViewById(R.id.date);
            contact_name=itemView.findViewById(R.id.contact_name);
            contact_number=itemView.findViewById(R.id.contact_number);
            time=itemView.findViewById(R.id.time);
            iv_call=itemView.findViewById(R.id.iv_call);
        }
    }
}
