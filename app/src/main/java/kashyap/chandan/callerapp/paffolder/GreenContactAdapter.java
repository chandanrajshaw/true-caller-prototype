package kashyap.chandan.callerapp.paffolder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.callerapp.Contacts;
import kashyap.chandan.callerapp.CustomItemClickListener;
import kashyap.chandan.callerapp.R;
import kashyap.chandan.callerapp.paffolder.paresponses.GreenContactResponse;

public class GreenContactAdapter extends RecyclerView.Adapter<GreenContactAdapter.MyViewHolder> {
    Context context;
    List< GreenContactResponse.DataBean> greenContacts;
    CustomItemClickListener listener;
    public GreenContactAdapter(Context context, List< GreenContactResponse.DataBean> greenContacts, CustomItemClickListener listener) {
        this.context=context;
        this.greenContacts=greenContacts;
        this.listener=listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.rejected_list_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.contact_number.setText(greenContacts.get(position).getPhone());
        holder.contact_name.setText(greenContacts.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return greenContacts.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_delete;
        TextView contact_name,contact_number;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            contact_name=itemView.findViewById(R.id.contact_name);
            contact_number=itemView.findViewById(R.id.contact_number);
            iv_delete =itemView.findViewById(R.id.ic_delete_rejected);
            View.OnClickListener clickListener=new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(view,greenContacts.get(getAdapterPosition()).getId(),String.valueOf(getAdapterPosition()),greenContacts.get(getAdapterPosition()).getPhone());
                }
            };
           iv_delete.setOnClickListener(clickListener);
        }

    }
    void updateList(int position)
    {
        greenContacts.remove(position);
        notifyDataSetChanged();
    }
}
