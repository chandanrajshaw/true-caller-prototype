package kashyap.chandan.callerapp.paffolder;

import android.app.Dialog;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.callerapp.Contacts;
import kashyap.chandan.callerapp.CustomItemClickListener;
import kashyap.chandan.callerapp.R;
import kashyap.chandan.callerapp.SharedPreferenceData;
import kashyap.chandan.callerapp.database.DataBaseHandler;
import kashyap.chandan.callerapp.paffolder.paresponses.BlueContactResponse;
import kashyap.chandan.callerapp.paffolder.paresponses.ContactStatusChangeResponse;
import kashyap.chandan.callerapp.retrofit.APIInterface;
import kashyap.chandan.callerapp.retrofit.ApiClient;
import kashyap.chandan.callerapp.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class BlueFragment extends Fragment {
    private RecyclerView contactRecycler;
List<BlueContactResponse.DataBean>blueContacts=new ArrayList<>();
    Dashboard dashboard;
    TextView tvNotice;
    SharedPreferenceData preferenceData;
    Dialog dialog;
    BlueContactAdapter adapter;
    DataBaseHandler dataBaseHandler;
    public BlueFragment(Dashboard dashboard) {

        this.dashboard = dashboard;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View view= inflater.inflate(R.layout.blue_fragment,container,false);
        contactRecycler=view.findViewById(R.id.contactRecycler);
        tvNotice=view.findViewById(R.id.tvNotice);
        preferenceData=new SharedPreferenceData(dashboard);
        dataBaseHandler=new DataBaseHandler(dashboard);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        contactRecycler.setLayoutManager(new LinearLayoutManager(dashboard,LinearLayoutManager.VERTICAL,false));
    }

    @Override
    public void onResume() {
        super.onResume();
        getBlueContacts();
    }

    private void getBlueContacts()
    {
        contactRecycler.setAdapter(null);
        dialog=new Dialog(dashboard);
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<BlueContactResponse> call=apiInterface.getBlueContact(preferenceData.getId());
        call.enqueue(new Callback<BlueContactResponse>() {
            @Override
            public void onResponse(Call<BlueContactResponse> call, Response<BlueContactResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    tvNotice.setVisibility(View.GONE);
                    contactRecycler.setVisibility(View.VISIBLE);
                    blueContacts=response.body().getData();
                    adapter=new BlueContactAdapter(dashboard, blueContacts, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, String id, String position,String phone) {
                        changeContactStatus(id,position,phone);
                        }
                    });

                    for(int i=0;i<blueContacts.size();i++)
                    {
                        BlueContactResponse.DataBean contacts=blueContacts.get(i);
                        Contacts con=new Contacts();
                        con.setName(contacts.getName());
                        con.setPhone(contacts.getPhone());
                        dataBaseHandler.addBlueContact(con);
                    }
                    contactRecycler.setAdapter(adapter);
                }
                else
                {
                    tvNotice.setVisibility(View.VISIBLE);
                    contactRecycler.setVisibility(View.GONE);
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        showToast(status.getMessage());
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }
            @Override
            public void onFailure(Call<BlueContactResponse> call, Throwable t) {
showToast(""+t.getMessage());
            }
        });
    }
    private void changeContactStatus(String id, String position,String phone) {
        dialog=new Dialog(dashboard);
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<ContactStatusChangeResponse> call=apiInterface.changeStatus(id,preferenceData.getId());
        call.enqueue(new Callback<ContactStatusChangeResponse>() {
            @Override
            public void onResponse(Call<ContactStatusChangeResponse> call, Response<ContactStatusChangeResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    adapter.updateList(Integer.parseInt(position));
                    showToast("Removed From Blue Contact");
                    int reject= dataBaseHandler.deleteBlue(phone);
                    if (reject>0)
                        Toast.makeText(dashboard, "Deleted from Local", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(dashboard, "Not Deleted from Local", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        showToast(status.getMessage());
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<ContactStatusChangeResponse> call, Throwable t) {
dialog.dismiss();showToast(t.getMessage());
            }
        });


    }
    public void showToast(String text)
    {
        Toast toast= Toast.makeText(dashboard,
                ""+text, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP| Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }
}
