package kashyap.chandan.callerapp.paffolder.paresponses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class ContactStatusChangeResponse {


    @Expose
    @SerializedName("status")
    private StatusBean Status;

    public StatusBean getStatus() {
        return Status;
    }

    public void setStatus(StatusBean Status) {
        this.Status = Status;
    }

    public static class StatusBean {
        @Expose
        @SerializedName("message")
        private String Message;
        @Expose
        @SerializedName("code")
        private int Code;

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }

        public int getCode() {
            return Code;
        }

        public void setCode(int Code) {
            this.Code = Code;
        }
    }
}
