package kashyap.chandan.callerapp.paffolder;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int num;
    Context context;
    Dashboard dashboard;
    public PagerAdapter(@NonNull FragmentManager fm, int behavior, int num
    , Context context) {
        super(fm, behavior);
        this.num=num;
        this.context=context;
        dashboard= (Dashboard) context;
    }
    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                MyContactFragment myContactFragment = new MyContactFragment(dashboard);
                return myContactFragment;
            case 1:
                GreenFragment fragmentB = new GreenFragment(dashboard);
                return fragmentB;
            case 2:
                BlueFragment fragmentC=new BlueFragment(dashboard);
                return fragmentC;
            case 3:
                RedFragment spamFragment=new RedFragment(dashboard);
                return  spamFragment;
            case 4:
                NotificationFragment notificationFragment=new NotificationFragment(dashboard);
                return  notificationFragment;
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return num;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }
}
