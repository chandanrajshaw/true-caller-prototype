package kashyap.chandan.callerapp.paffolder.paresponses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class BlueContactResponse {


    @Expose
    @SerializedName("data")
    private List<DataBean> Data;
    @Expose
    @SerializedName("status")
    private StatusBean Status;

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public StatusBean getStatus() {
        return Status;
    }

    public void setStatus(StatusBean Status) {
        this.Status = Status;
    }

    public static class DataBean implements Serializable {
        @Expose
        @SerializedName("date_time")
        private String DateTime;
        @Expose
        @SerializedName("status")
        private String Status;
        @Expose
        @SerializedName("phone")
        private String Phone;
        @Expose
        @SerializedName("name")
        private String Name;
        @Expose
        @SerializedName("customer_id")
        private String CustomerId;
        @Expose
        @SerializedName("id")
        private String Id;

        public String getDateTime() {
            return DateTime;
        }

        public void setDateTime(String DateTime) {
            this.DateTime = DateTime;
        }

        public String getStatus() {
            return Status;
        }

        public void setStatus(String Status) {
            this.Status = Status;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String Phone) {
            this.Phone = Phone;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public String getCustomerId() {
            return CustomerId;
        }

        public void setCustomerId(String CustomerId) {
            this.CustomerId = CustomerId;
        }

        public String getId() {
            return Id;
        }

        public void setId(String Id) {
            this.Id = Id;
        }
    }

    public static class StatusBean implements Serializable{
        @Expose
        @SerializedName("message")
        private String Message;
        @Expose
        @SerializedName("code")
        private int Code;

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }

        public int getCode() {
            return Code;
        }

        public void setCode(int Code) {
            this.Code = Code;
        }
    }
}
