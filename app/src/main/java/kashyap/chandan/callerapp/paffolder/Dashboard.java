package kashyap.chandan.callerapp.paffolder;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;

import kashyap.chandan.callerapp.R;
import kashyap.chandan.callerapp.profile.MySettingPage;

public class Dashboard extends AppCompatActivity  {
TabLayout tabLayout;
ViewPager viewPager;
public  Toolbar toolbar;
TextView tvTitle;
static ImageView iv_back;
    MyContactFragment fragment;
//    PendingIntent pendingIntent;
//    Intent intent;
//ContactAdapter contactAdapter;
//  public    boolean is_in_action_mode=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        init();
        setSupportActionBar(toolbar);

        TabLayout.Tab tabmyContact= tabLayout.newTab();
        tabmyContact.setText("Contact")/*.setIcon(R.drawable.ic_contact_book)*/;
        tabLayout.addTab(tabmyContact,0,true);
        TabLayout.Tab green=tabLayout.newTab();
        green.setText("Green")/*.setIcon(R.drawable.ic_star)*/;
        tabLayout.addTab(green,1);
        TabLayout.Tab blue= tabLayout.newTab();
        blue.setText("Blue")/*.setIcon(R.drawable.ic_suitcase)*/;
        tabLayout.addTab(blue,2);
        TabLayout.Tab red= tabLayout.newTab();
        red.setText("Red")/*.setIcon(R.drawable.ic_spam)*/;
        tabLayout.addTab(red,3);
        TabLayout.Tab notification= tabLayout.newTab();
        notification.setIcon(R.drawable.ic_notifications)/*.setIcon(R.drawable.ic_spam)*/;
        tabLayout.addTab(notification,4);
        PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT,tabLayout.getTabCount(),Dashboard.this);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
//        intent=new Intent(Dashboard.this,IncomingCallReceiver.class);
//        intent.setAction("android.intent.action.PHONE_STATE");
//        intent.putExtra(TelephonyManager.EXTRA_STATE,TelephonyManager.EXTRA_STATE_RINGING);
//        sendBroadcast(intent);
//        PendingIntent pendIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//        iv_back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                MyContactFragment.is_in_action_mode=false;
//                fragment.adapter.notifyDataSetChanged();
//                iv_back.setVisibility(View.GONE);
//                toolbar.getMenu().clear();
//            }
//        });
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
               if (tabmyContact.isSelected())
               {
                toolbar.setVisibility(View.VISIBLE);
               viewPager.setCurrentItem(tab.getPosition());
               }
               else if (green.isSelected())
               {
                   toolbar.setVisibility(View.VISIBLE);
                   viewPager.setCurrentItem(tab.getPosition());
               }
               else if (blue.isSelected())
               {
                   toolbar.setVisibility(View.GONE);
                   viewPager.setCurrentItem(tab.getPosition());
               }
               else if (red.isSelected())
               {
                   toolbar.setVisibility(View.GONE);
                   viewPager.setCurrentItem(tab.getPosition());
               }
               else if (notification.isSelected())
               {
                   toolbar.setVisibility(View.GONE);
                   int tabIconColor = ContextCompat.getColor(getApplicationContext(), R.color.white);
                   notification.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                   viewPager.setCurrentItem(tab.getPosition());
               }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
if (!tabmyContact.isSelected())
{
    MyContactFragment.is_in_action_mode=false;
   toolbar.getMenu().clear();
   toolbar.inflateMenu(R.menu.options_menu);
   iv_back.setVisibility(View.GONE);
    fragment.adapter.notifyDataSetChanged();
}
else if (!notification.isSelected())
{
}
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if (!tabmyContact.isSelected())
                {
                    MyContactFragment.is_in_action_mode=false;
                    toolbar.getMenu().clear();
//                    toolbar.inflateMenu(R.menu.options_menu);
                    iv_back.setVisibility(View.GONE);
                    fragment.adapter.notifyDataSetChanged();
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (MyContactFragment.is_in_action_mode)
        {
            MyContactFragment.is_in_action_mode=false;
            toolbar.getMenu().clear();
            toolbar.inflateMenu(R.menu.options_menu);
        }


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.options_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.profileSetting:
                Intent intent=new Intent(Dashboard.this, MySettingPage.class);
                startActivity(intent);
                return true;
        }
        return false;
    }

    private void init() {
        fragment=new MyContactFragment(Dashboard.this);
        toolbar=findViewById(R.id.toolbar);
        iv_back=findViewById(R.id.iv_back);
        tabLayout=findViewById(R.id.tabLayout);
        viewPager=findViewById(R.id.viewPager);
        tvTitle=findViewById(R.id.tvTitle);
    }

}