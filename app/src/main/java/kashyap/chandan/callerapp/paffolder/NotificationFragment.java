package kashyap.chandan.callerapp.paffolder;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.callerapp.CustomItemClickListener;
import kashyap.chandan.callerapp.R;
import kashyap.chandan.callerapp.database.DataBaseHandler;
import kashyap.chandan.callerapp.database.Notifications;

public class NotificationFragment extends Fragment {
    private RecyclerView contactRecycler;
    List<Notifications>notifications=new ArrayList<>();
    Dashboard dashboard;
    DataBaseHandler db;
    TextView tvNotice;

    public NotificationFragment(Dashboard dashboard) {
        this.dashboard = dashboard;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View view= inflater.inflate(R.layout.my_notification_fragment,container,false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        contactRecycler=view.findViewById(R.id.contactRecycler);
        tvNotice=view.findViewById(R.id.tvNotice);
//       dashboard.toolbar.setVisibility(View.GONE);
        db=new DataBaseHandler(dashboard);
        notifications=db.getNotificationTable();
        if (notifications.size()<=0)
        {
            tvNotice.setVisibility(View.VISIBLE);
            tvNotice.setText("No Notification");
            contactRecycler.setVisibility(View.GONE);
        }
        else
        {
            tvNotice.setVisibility(View.GONE);
            contactRecycler.setVisibility(View.VISIBLE);
            contactRecycler.setLayoutManager(new LinearLayoutManager(dashboard,LinearLayoutManager.VERTICAL,false));
            contactRecycler.setAdapter(new NotificationAdapter(dashboard,notifications, new CustomItemClickListener() {
                @Override
                public void onItemClick(View v, String id, String value,String phone) {

                }
            }));
        }
    }


    @Override
    public void onResume() {
        super.onResume();



    }


}
