package kashyap.chandan.callerapp.paffolder;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.callerapp.Contacts;
import kashyap.chandan.callerapp.R;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.MyViewHolder> {
    Context context;
    public static List<Contacts> selectUsers;
    ArrayList<Contacts>selectedContacts=new ArrayList<>();
    private SparseBooleanArray mSelectedItemsIds;
    Dashboard dashboard;
MyContactFragment fragment;
    public ContactAdapter(Context context) {
        this.context = context;
        this.dashboard= (Dashboard) context;
        fragment=new MyContactFragment(context);
    }
    public ContactAdapter(Context context, List<Contacts> selectUsers) {
        this.selectUsers=selectUsers;
        this.context=context;
        this.dashboard= (Dashboard) context;
        fragment=new MyContactFragment(context);
        mSelectedItemsIds = new SparseBooleanArray();

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.contact_list_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
holder.contact_name.setText(selectUsers.get(position).getName());
holder.contact_number.setText(selectUsers.get(position).getPhone());
        if (!MyContactFragment.is_in_action_mode)
        {
            holder.checkBox.setVisibility(View.GONE);
        }
        else
        {
            holder.checkBox.setVisibility(View.VISIBLE);
            holder.checkBox.setChecked(false);
        }
    }

    @Override
    public int getItemCount() {
        return selectUsers.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder /*implements View.OnClickListener*/ {
        TextView contact_name,contact_number;
        CheckBox checkBox;
        RelativeLayout layout;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            contact_number=itemView.findViewById(R.id.contact_number);
            contact_name=itemView.findViewById(R.id.contact_name);
            checkBox=itemView.findViewById(R.id.checkBox);
            layout=itemView.findViewById(R.id.layout);
//            View.OnLongClickListener longClickListener=new View.OnLongClickListener() {
//                @Override
//                public boolean onLongClick(View view) {
//                    dashboard.is_in_action_mode=true;
//                    notifyDataSetChanged();
//                    return true;
//                }
//            };
            layout.setOnLongClickListener(fragment);
//            checkBox.setOnClickListener(this);

        }

//        @Override
//        public void onClick(View view) {
//fragment.prepareSelection(view,selectUsers.get(getAdapterPosition()));
//        }
    }
    //Toggle selection methods
    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }
//    public void updateAdapter(List<Contacts> list)
//    {
//        for (Contacts contact:list)
//        {
//            selectUsers.remove(contact);
//        }
//
//    }

    //Remove selected selections
    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }


    //Put or delete selected position into SparseBooleanArray
    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);

        notifyDataSetChanged();
    }

    //Get total selected count
    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    //Return all selected ids
    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

}
