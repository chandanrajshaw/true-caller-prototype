package kashyap.chandan.callerapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Contacts {
    @Expose
    @SerializedName("id")
    private String id;
@Expose
    @SerializedName("name")
    private String name;
@Expose
    @SerializedName("phone")
    private String phone;
    public Contacts() {
    }

    public Contacts(int id, String name, String phone) {
this.name=name;
this.phone=phone;
this.id=String.valueOf(id);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
