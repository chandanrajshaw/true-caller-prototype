package kashyap.chandan.callerapp.Admin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.callerapp.Admin.AdminResponse.AddCustomerResponse;
import kashyap.chandan.callerapp.Admin.AdminResponse.CustomerListResponse;
import kashyap.chandan.callerapp.CustomItemClickListener;
import kashyap.chandan.callerapp.R;
import kashyap.chandan.callerapp.profile.MySettingPage;
import kashyap.chandan.callerapp.retrofit.APIInterface;
import kashyap.chandan.callerapp.retrofit.ApiClient;
import kashyap.chandan.callerapp.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class AdminDashBoard extends AppCompatActivity implements View.OnClickListener {
CircleImageView fab;
RecyclerView customerRecycler;
Thread thread;
Dialog dialog;
Toolbar toolbar;
CustomerListAdapter adapter;
List< CustomerListResponse.DataBean>customers;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_dash_board);
        init();
        setSupportActionBar(toolbar);
    }
    @Override
    protected void onResume() {
        super.onResume();
   getCustomer();

    fab.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent=new Intent(AdminDashBoard.this,AddCustomer.class);
            startActivity(intent);
        }
    });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.options_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.profileSetting:
                Intent intent=new Intent(AdminDashBoard.this, MySettingPage.class);
                startActivity(intent);
                break;
        }
        return true;
    }

    private void init() {
        toolbar=findViewById(R.id.toolbar);
        customers=new ArrayList<>();
        dialog=new Dialog(AdminDashBoard.this);
        fab=findViewById(R.id.fab);
        customerRecycler=findViewById(R.id.customerRecycler);
        customerRecycler.setLayoutManager(new LinearLayoutManager(AdminDashBoard.this,LinearLayoutManager.VERTICAL,false));

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.fab:
                Intent intent=new Intent(AdminDashBoard.this,AddCustomer.class);
                startActivity(intent);
                break;
        }
    }
    private void getCustomer()
    {
        customers.clear();
        adapter=null;
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<CustomerListResponse> call=apiInterface.getCustomerList();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<CustomerListResponse>() {
                    @Override
                    public void onResponse(Call<CustomerListResponse> call, Response<CustomerListResponse> response) {
                        if (response.code()==200)
                        { dialog.dismiss();
                            customers=response.body().getData();
                            adapter=new CustomerListAdapter(AdminDashBoard.this,customers, new CustomItemClickListener() {
                                @Override
                                public void onItemClick(View v, String id, String value,String phone) {

                                }
                            });


                            customerRecycler.setAdapter(adapter);
                        }
                        else
                        {
                            dialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                showToast(status.getMessage());
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<CustomerListResponse> call, Throwable t) {
                        dialog.dismiss();
                        showToast(t.getLocalizedMessage());
                    }
                });
            }
        };
        thread=new Thread(runnable);
        thread.start();
    }
    public void showToast(String text)
    {
        Toast toast= Toast.makeText(getApplicationContext(),
                ""+text, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP| Gravity.CENTER, 0, 0);
        toast.show();
    }
}