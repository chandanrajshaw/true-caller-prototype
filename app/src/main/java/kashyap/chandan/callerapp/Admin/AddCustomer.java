package kashyap.chandan.callerapp.Admin;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.regex.Pattern;

import kashyap.chandan.callerapp.Admin.AdminResponse.AddCustomerResponse;
import kashyap.chandan.callerapp.MainActivity;
import kashyap.chandan.callerapp.R;
import kashyap.chandan.callerapp.customer.CustomerDashBoard;
import kashyap.chandan.callerapp.loginResponses.LoginResponse;
import kashyap.chandan.callerapp.retrofit.APIInterface;
import kashyap.chandan.callerapp.retrofit.ApiClient;
import kashyap.chandan.callerapp.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class AddCustomer extends AppCompatActivity implements View.OnClickListener {
ImageView iv_back;
TextView btnAdd;
TextInputEditText et_name,et_email,et_phone;
Dialog dialog;
Thread thread;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_customer);
        init();
        btnAdd.setOnClickListener(this);
    }

    private void init() {
        dialog=new Dialog(AddCustomer.this);
        iv_back=findViewById(R.id.iv_back);
        btnAdd=findViewById(R.id.btnAdd);
        et_name=findViewById(R.id.et_name);
        et_email=findViewById(R.id.et_email);
        et_phone=findViewById(R.id.et_phone);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.iv_back:
               finish();
               break;
            case R.id.btnAdd:
                addCustomer();
                break;
        }
    }
    private void addCustomer()
    {
        String username=et_name.getText().toString().trim();
        String email=et_email.getText().toString().trim();
        String phone=et_phone.getText().toString().trim();
        if (username.isEmpty()&&email.isEmpty()&&phone.isEmpty())
            showToast("Fill All the fields");
        else if (username.isEmpty()) {
            showToast("Enter Valid Email");
            et_name.requestFocus();
        }
        else if (!emailValidation(email)) {
            showToast("Enter Valid Email");
            et_email.requestFocus();
        }
        else if (phone.isEmpty()||phone.length()!=10) {
            showToast("Enter Password");
            et_phone.requestFocus();
        }
        else
        {
            dialog.setContentView(R.layout.loadingdialog);
            dialog.setCancelable(true);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
            Call<AddCustomerResponse> call=apiInterface.add_customer(username,email,phone);
            Runnable runnable=new Runnable() {
                @Override
                public void run() {
                    call.enqueue(new Callback<AddCustomerResponse>() {
                        @Override
                        public void onResponse(Call<AddCustomerResponse> call, Response<AddCustomerResponse> response) {
                            if (response.code()==200)
                            {
                                showToast("Customer Added");
                                finish();
                            }
                            else
                            {
                                dialog.dismiss();
                                Converter<ResponseBody, ApiError> converter =
                                        ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                ApiError error;
                                try {
                                    error = converter.convert(response.errorBody());
                                    ApiError.StatusBean status=error.getStatus();
                                    showToast(status.getMessage());
                                } catch (IOException e) { e.printStackTrace(); }
                            }
                        }

                        @Override
                        public void onFailure(Call<AddCustomerResponse> call, Throwable t) {
                            dialog.dismiss();
                            showToast(t.getLocalizedMessage());
                        }
                    });
                }
            };
            thread=new Thread(runnable);
            thread.start();
        }

    }
    public void showToast(String text)
    {
        Toast toast= Toast.makeText(getApplicationContext(),
                ""+text, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP| Gravity.CENTER, 0, 0);
        toast.show();
    }
    public boolean emailValidation(String email)
    {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email.isEmpty()||email == null)
            return false;
        return pat.matcher(email).matches();
    }
}