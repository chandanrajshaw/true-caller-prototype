package kashyap.chandan.callerapp.Admin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.callerapp.Admin.AdminResponse.CustomerListResponse;
import kashyap.chandan.callerapp.CustomItemClickListener;
import kashyap.chandan.callerapp.R;
import kashyap.chandan.callerapp.retrofit.ApiClient;

public class CustomerListAdapter extends RecyclerView.Adapter<CustomerListAdapter.MyViewHolder> {
    Context context;
    CustomItemClickListener listener;
    List<CustomerListResponse.DataBean> customers;
    public CustomerListAdapter(Context context, List<CustomerListResponse.DataBean> customers, CustomItemClickListener listener) {
        this.context=context;
        this.listener=listener;
        this.customers=customers;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.customer_list_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
holder.tv_user_name.setText(customers.get(position).getUsername());
holder.tv_email.setText(customers.get(position).getEmail());
holder.tv_phone.setText(customers.get(position).getPhone());
Picasso.get().load(ApiClient.IMAGE_URL+customers.get(position).getImage()).placeholder(R.drawable.loading).error(R.drawable.ic_broken_image).into(holder.customerImage);
    }

    @Override
    public int getItemCount() {
        return customers.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_user_name,tv_phone,tv_email;
        CircleImageView customerImage,delete,iv_call;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_user_name=itemView.findViewById(R.id.tv_user_name);
            tv_phone=itemView.findViewById(R.id.tv_phone);
            tv_email=itemView.findViewById(R.id.tv_email);
            customerImage=itemView.findViewById(R.id.customerImage);
            delete=itemView.findViewById(R.id.red);
            iv_call=itemView.findViewById(R.id.iv_call);

        }
    }
}
