package kashyap.chandan.callerapp.customer;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class CustomerPagerAdapter extends FragmentStatePagerAdapter {
    int num;
    Context context;
    CustomerDashBoard dashboard;

    public CustomerPagerAdapter(@NonNull FragmentManager fm, int behavior,int num,Context context) {
        super(fm, behavior);
        this.num=num;
        this.context=context;
        dashboard= (CustomerDashBoard) context;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                CustomerContactFragment customerContactFragment = new CustomerContactFragment(dashboard);
                return customerContactFragment;
            case 1:
                PAFragment paFragment = new PAFragment(dashboard);
                return paFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return num;
    }
}
