package kashyap.chandan.callerapp.customer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public  class GetPAResponse implements Serializable {
    @Expose
    @SerializedName("data")
    private List<DataBean> Data;
    @Expose
    @SerializedName("status")
    private StatusBean Status;
    public List<DataBean> getData() {
        return Data;
    }
    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public StatusBean getStatus() {
        return Status;
    }

    public void setStatus(StatusBean Status) {
        this.Status = Status;
    }

    public static class DataBean implements Serializable {
        @Expose
        @SerializedName("date_time_user")
        private String Date_time_user;
        @Expose
        @SerializedName("status")
        private String Status;
        @Expose
        @SerializedName("role")
        private String Role;
        @Expose
        @SerializedName("otp")
        private String Otp;
        @Expose
        @SerializedName("image")
        private String Image;
        @Expose
        @SerializedName("u_id")
        private String U_id;
        @Expose
        @SerializedName("password")
        private String Password;
        @Expose
        @SerializedName("phone")
        private String Phone;
        @Expose
        @SerializedName("email")
        private String Email;
        @Expose
        @SerializedName("username")
        private String Username;
        @Expose
        @SerializedName("customer_id")
        private String Customer_id;
        @Expose
        @SerializedName("id")
        private String Id;

        public String getDate_time_user() {
            return Date_time_user;
        }

        public void setDate_time_user(String Date_time_user) {
            this.Date_time_user = Date_time_user;
        }

        public String getStatus() {
            return Status;
        }

        public void setStatus(String Status) {
            this.Status = Status;
        }

        public String getRole() {
            return Role;
        }

        public void setRole(String Role) {
            this.Role = Role;
        }

        public String getOtp() {
            return Otp;
        }

        public void setOtp(String Otp) {
            this.Otp = Otp;
        }

        public String getImage() {
            return Image;
        }

        public void setImage(String Image) {
            this.Image = Image;
        }

        public String getU_id() {
            return U_id;
        }

        public void setU_id(String U_id) {
            this.U_id = U_id;
        }

        public String getPassword() {
            return Password;
        }

        public void setPassword(String Password) {
            this.Password = Password;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String Phone) {
            this.Phone = Phone;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getUsername() {
            return Username;
        }

        public void setUsername(String Username) {
            this.Username = Username;
        }

        public String getCustomer_id() {
            return Customer_id;
        }

        public void setCustomer_id(String Customer_id) {
            this.Customer_id = Customer_id;
        }

        public String getId() {
            return Id;
        }

        public void setId(String Id) {
            this.Id = Id;
        }
    }

    public static class StatusBean {
        @Expose
        @SerializedName("message")
        private String Message;
        @Expose
        @SerializedName("code")
        private int Code;

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }

        public int getCode() {
            return Code;
        }

        public void setCode(int Code) {
            this.Code = Code;
        }
    }
}
