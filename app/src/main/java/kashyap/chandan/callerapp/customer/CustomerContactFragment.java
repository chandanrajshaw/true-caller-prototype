package kashyap.chandan.callerapp.customer;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.callerapp.Contacts;
import kashyap.chandan.callerapp.R;
import kashyap.chandan.callerapp.SharedPreferenceData;
import kashyap.chandan.callerapp.retrofit.APIInterface;
import kashyap.chandan.callerapp.retrofit.ApiClient;
import kashyap.chandan.callerapp.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class CustomerContactFragment extends Fragment {
   static public List<Contacts> selectUsers = new ArrayList<>();
    ArrayList<Contacts> contacts = new ArrayList<>();
    ArrayList<Contacts> contactsend = new ArrayList<>();
    Cursor phones;
    public ArrayList<Contacts> removed = new ArrayList<>();
    Context context;
    CustomerContactAdapter adapter;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    CustomerDashBoard dashBoard;
    SharedPreferenceData preferenceData;
    static String id;
RecyclerView contactRecycler;
Dialog dialog;
    public CustomerContactFragment(Context context) {
        this.context = context;
        this.dashBoard= (CustomerDashBoard) context;
        preferenceData=new SharedPreferenceData(dashBoard);

    }

    public CustomerContactFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.my_contact_fragment,container,false);
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        contactRecycler=view.findViewById(R.id.contactRecycler);
    }

    @Override
    public void onResume() {
        super.onResume();
        selectUsers.clear();
        showContacts();
        id= preferenceData.getId();
        adapter = new CustomerContactAdapter(getContext(), selectUsers);
        contactRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        contactRecycler.setAdapter(adapter);
        sendContactList();

    }

    private void sendContactList() {
//        JSONArray jsonArray=new JSONArray();
        List<String> name=new ArrayList<>();
        List<String> phone=new ArrayList<>();
        for (int i=0;i<selectUsers.size();i++)
        {
            Contacts contacts=selectUsers.get(i);
            name.add(contacts.getName());
            phone.add(contacts.getPhone());
//            JSONObject object=new JSONObject();
//            try {
//                object.put("id",preferenceData.getId());
//                object.put("name",contacts.getName());
//                object.put("number",contacts.getPhone());
//                jsonArray.put(object);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }

//            contactsend.add(new Contacts(Integer.parseInt(preferenceData.getId()),contacts.getName(),contacts.getPhone()));
                 }
//        JSONObject jsonObject=new JSONObject();
//        try {
//            jsonObject.put("contact_list",jsonArray);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

//        JsonObject objects= JsonParser.parseString( jsonObject.toString()).getAsJsonObject();
//        JsonObject objects=new JsonParser().parse(jsonObject.toString()).getAsJsonObject();
        dialog=new Dialog(dashBoard);
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        System.out.println(preferenceData.getId());
        Call<SendContactResponse>call=apiInterface.addContacts(name,phone,id);
        call.enqueue(new Callback<SendContactResponse>() {
            @Override
            public void onResponse(Call<SendContactResponse> call, Response<SendContactResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    Toast.makeText(dashBoard, "Contact Saved Successfully", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(dashBoard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<SendContactResponse> call, Throwable t) {
dialog.dismiss();
                Toast.makeText(dashBoard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showContacts() {
        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getContext().checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
        } else {
            phones = getContext().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
            LoadContacts loadContact = new CustomerContactFragment.LoadContacts();
            loadContact.execute();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                showContacts();
            } else {
                Snackbar.make(getActivity().getWindow().getDecorView().findViewById(android.R.id.content), "Until you grant the permission, we canot display the names", Snackbar.LENGTH_SHORT).show();
            }
        }
    }


    class LoadContacts extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... voids) {

            if (phones != null) {
                Log.e("count", "" + phones.getCount());
                if (phones.getCount() == 0) {

                }

                while (phones.moveToNext()) {
                    Bitmap bit_thumb = null;
                    String id = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                    String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    Contacts selectUser = new Contacts();

                    selectUser.setName(name);
                    selectUser.setPhone(phoneNumber);
                    selectUsers.add(selectUser);
                }

            } else {
                Log.e("Cursor close 1", "----------------");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // sortContacts();
            int count = selectUsers.size();
            for (int i = 0; i < selectUsers.size(); i++) {
                Contacts inviteFriendsProjo = selectUsers.get(i);

                if (inviteFriendsProjo.getName().matches("\\d+(?:\\.\\d+)?") || inviteFriendsProjo.getName().trim().length() == 0) {
                    removed.add(inviteFriendsProjo);
                } else {
                    contacts.add(inviteFriendsProjo);
                }
            }


        }
    }
}
