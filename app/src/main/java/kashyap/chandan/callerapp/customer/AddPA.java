package kashyap.chandan.callerapp.customer;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.regex.Pattern;

import kashyap.chandan.callerapp.R;
import kashyap.chandan.callerapp.SharedPreferenceData;
import kashyap.chandan.callerapp.retrofit.APIInterface;
import kashyap.chandan.callerapp.retrofit.ApiClient;
import kashyap.chandan.callerapp.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class AddPA extends AppCompatActivity {
ImageView iv_back;
TextView btnadd;
    Dialog dialog;
TextInputEditText et_name,et_email,et_phone;
SharedPreferenceData preferenceData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_p);
        init();

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addPa();
            }
        });
    }

    private void init() {
        btnadd=findViewById(R.id.btnadd);
        preferenceData=new SharedPreferenceData(AddPA.this);
        dialog=new Dialog(AddPA.this);
        iv_back=findViewById(R.id.iv_back);
        et_name=findViewById(R.id.et_name);
        et_email=findViewById(R.id.et_email);
        et_phone=findViewById(R.id.et_phone);
    }
private void addPa()
{
    String email=et_email.getText().toString().trim();
    String name=et_name.getText().toString().trim();
    String phone=et_phone.getText().toString().trim();
    if (name.isEmpty()&&email.isEmpty()&&phone.isEmpty())
        showToast("Fill All the fields");
    else if (name.isEmpty())
        showToast("Enter Name");
    else if (!emailValidation(email))
        showToast("Enter Valid Email");
    else if (phone.isEmpty())
        showToast("Enter Valid Phone");
    else
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<AddPAResponse>call=apiInterface.add_Pa(name,email,phone,preferenceData.getId());
        call.enqueue(new Callback<AddPAResponse>() {
            @Override
            public void onResponse(Call<AddPAResponse> call, Response<AddPAResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    showToast("PA Added ");
                    finish();
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        showToast(status.getMessage());
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<AddPAResponse> call, Throwable t) {
showToast(t.getLocalizedMessage());
            }
        });
    }
}
    public void showToast(String text)
    {
        Toast toast= Toast.makeText(getApplicationContext(),
                ""+text, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP| Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }
    public boolean emailValidation(String email)
    {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email.isEmpty()||email == null)
            return false;
        return pat.matcher(email).matches();
    }
}