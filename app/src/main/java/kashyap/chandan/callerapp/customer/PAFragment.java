package kashyap.chandan.callerapp.customer;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.callerapp.R;
import kashyap.chandan.callerapp.SharedPreferenceData;
import kashyap.chandan.callerapp.logins.GetOtpResponse;
import kashyap.chandan.callerapp.retrofit.APIInterface;
import kashyap.chandan.callerapp.retrofit.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PAFragment extends Fragment {
    Context context;
    TextView tvPaName,tvpaEmail,tvPaPhone,not_available;
    CustomerDashBoard dashBoard;
    Dialog dialog;
    LinearLayout detail;
    SharedPreferenceData sharedPreferenceData;
CircleImageView fab,paImage;
    public PAFragment(Context context) {
        this.context = context;
        this.dashBoard= (CustomerDashBoard) context;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.pa_fragment,container,false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fab=view.findViewById(R.id.fab);
        tvpaEmail=view.findViewById(R.id.tvpaEmail);
        tvPaName=view.findViewById(R.id.tvPaName);
        tvPaPhone=view.findViewById(R.id.tvPaPhone);
        not_available=view.findViewById(R.id.not_available);
        fab=view.findViewById(R.id.fab);
        paImage=view.findViewById(R.id.paImage);
        detail=view.findViewById(R.id.detail);
        dialog=new Dialog(dashBoard);
        sharedPreferenceData=new SharedPreferenceData(dashBoard);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context,AddPA.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getPa();
    }

    private void getPa()
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<GetPAResponse>call=apiInterface.getPA(sharedPreferenceData.getId());
        call.enqueue(new Callback<GetPAResponse>() {
            @Override
            public void onResponse(Call<GetPAResponse> call, Response<GetPAResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    List<GetPAResponse.DataBean>dataBeans=response.body().getData();
                    GetPAResponse.DataBean data=dataBeans.get(0);
                    fab.setVisibility(View.GONE);
                    detail.setVisibility(View.VISIBLE);
                    not_available.setVisibility(View.GONE);
                    Picasso.get().load(ApiClient.IMAGE_URL+data.getImage()).placeholder(R.drawable.loading)
                            .error(R.drawable.ic_broken_image).into(paImage);
                    tvPaName.setText(data.getUsername());
                    tvpaEmail.setText(data.getEmail());
                    tvPaPhone.setText(data.getPhone());
                }
                else
                {
                    dialog.dismiss();
                    fab.setVisibility(View.VISIBLE);
                    detail.setVisibility(View.GONE);
                    not_available.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onFailure(Call<GetPAResponse> call, Throwable t) {
dialog.dismiss();
                Toast.makeText(dashBoard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

}
