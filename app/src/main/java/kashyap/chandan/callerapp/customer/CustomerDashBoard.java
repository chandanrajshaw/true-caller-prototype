package kashyap.chandan.callerapp.customer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;

import kashyap.chandan.callerapp.R;
import kashyap.chandan.callerapp.profile.MySettingPage;

public class CustomerDashBoard extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager viewPager;
    public Toolbar toolbar;
    TextView tvTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_dash_board);
        init();
        setSupportActionBar(toolbar);
        TabLayout.Tab tabCustomerContact= tabLayout.newTab();
        tabCustomerContact.setText("Contact")/*.setIcon(R.drawable.ic_contact_book)*/;
        tabLayout.addTab(tabCustomerContact,0,true);
        TabLayout.Tab paTab=tabLayout.newTab();
        paTab.setText("PA")/*.setIcon(R.drawable.ic_star)*/;
        tabLayout.addTab(paTab);
        CustomerPagerAdapter adapter = new CustomerPagerAdapter
                (getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT,tabLayout.getTabCount(),CustomerDashBoard.this);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }
    private void init() {
        toolbar=findViewById(R.id.toolbar);
        tabLayout=findViewById(R.id.tabLayout);
        viewPager=findViewById(R.id.viewPager);
        tvTitle=findViewById(R.id.tvTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.options_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId())
        {
            case  R.id.profileSetting:
                Intent intent=new Intent(CustomerDashBoard.this, MySettingPage.class);
                startActivity(intent);
                break;
        }
        return true;
    }
}