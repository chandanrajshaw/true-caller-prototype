package kashyap.chandan.callerapp;

import android.view.View;

public interface CustomItemClickListener {
    public void onItemClick(View v, String id, String position,String phone);
}
