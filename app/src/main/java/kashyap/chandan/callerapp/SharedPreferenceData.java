package kashyap.chandan.callerapp;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class SharedPreferenceData {
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences1;
    SharedPreferences.Editor editor1;
    Context context;

    public SharedPreferenceData(Context context) {
        this.context = context;
        this.sharedPreferences= this.context.getSharedPreferences("Login",MODE_PRIVATE);
        this.editor=sharedPreferences.edit();
//        this.sharedPreferences1=context.getSharedPreferences("Splash",MODE_PRIVATE);
//        this.editor1=sharedPreferences1.edit();
    }
    public void putSharedPreference(String id,String username,String phone,String email,String role,String image,String date)
    {
        editor.putString("id",id);
        editor.putString("UName",username);
        editor.putString("phone",phone);
        editor.putString("role",role);
        editor.putString("email",email);
        editor.putString("image",image);
        editor.putString("date",date);
        editor.commit();
    }
    public void sessionEnd()
    {
        editor.clear();
        editor.commit();
    }
    public String getDate()
    {
        String date=sharedPreferences.getString("date",null);
        return date ;
    }
    public String getId()
    {
        String id=sharedPreferences.getString("id",null);
        return id ;
    }
    public String getImage()
    {
        String image=sharedPreferences.getString("image",null);
        return image ;
    }
    public String getEmail()
    {
        String email=sharedPreferences.getString("email",null);
        return email ;
    }
    public String getRole()
    {
        String role=sharedPreferences.getString("role",null);
        return role ;
    }
    public String getPhone()
    {
        String phone=sharedPreferences.getString("phone",null);
        return phone ;
    }
    public String getUserName()
    {
        String fname=sharedPreferences.getString("UName",null);
        return fname ;
    }

}
